#pragma once
#include "stdafx.h"
#include "ShaderBuffer.h"

template<typename Element>
class ShaderStorageBuffer : public ShaderBuffer<Element>
{
public:
	ShaderStorageBuffer() {};
	~ShaderStorageBuffer() {};

	void Init( unsigned int _capacity )
	{
		m_bufferType = GL_SHADER_STORAGE_BUFFER;
		ShaderBuffer::Init( _capacity );
	};

	void Bind( GLuint _program, std::string _nameInShader, GLuint _indexInShader )
	{
		GLuint bindPoint = glGetProgramResourceIndex( _program, GL_SHADER_STORAGE_BLOCK, _nameInShader.c_str() );
		glShaderStorageBlockBinding( _program, bindPoint, _indexInShader ); /* Connect the shader storage block to the SSBO. (Not needed if it is hard coded in the shader.)*/
		glBindBufferBase( GL_SHADER_STORAGE_BUFFER, _indexInShader, m_handle ); /* Binding the SSBO to a particular binding point.*/
	};

	void Add( Element& _element )
	{
		assert( m_elements.size() < m_capacity );
		m_elements.push_back( _element );
	};

	void AddVector( std::vector<Element> _vector )
	{
		assert( m_elements.size() + _vector.size() - 1 < m_capacity );
		m_elements.insert( m_elements.end(), _vector.begin(), _vector.end() );
	};

	void Update()
	{
		ShaderBuffer::Update( m_elements.data(), m_elements.size() );
	};

	std::vector<Element>& GetElements()
	{
		return m_elements;
	}

private:
	std::vector<Element>	m_elements;
};

