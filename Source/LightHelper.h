#pragma once
#include "stdafx.h"

struct Material
{
	glm::vec4 Ambient;
	glm::vec4 Diffuse;
	glm::vec4 Specular; 
	
	Material() {};
	Material(const glm::vec4& a, const glm::vec4& d, const glm::vec4& s) : Ambient(a), Diffuse(d), Specular(s) {};
	//glm::vec4 Reflect;
};

struct DirectionalLight
{
	glm::vec4 Ambient;
	glm::vec4 Diffuse;
	glm::vec4 Specular;
	glm::vec3 Direction;	float pad;
};

struct PointLight
{
	glm::vec4	Ambient;
	glm::vec4	Diffuse;
	glm::vec4	Specular;
	glm::vec3	Position;
	float		Range;
	glm::vec3	Att;		float pad;
};

struct SpotLight
{
	glm::vec4	Ambient;
	glm::vec4	Diffuse;
	glm::vec4	Specular;
	glm::vec3	Position;
	float		Range;
	glm::vec3	Direction;
	float		Spot;
	glm::vec3	Att;		float pad;
};