#ifndef AMOUSE_H
#define AMOUSE_H

#include "IMouseObserver.h"
#include <vector>

/* Not actually abstract, but not supposed to be instantiated.*/
class AMouse
{
public:
	enum MouseNotification
	{
		MouseMove,
		MouseMovePassive,
		MouseButton
	};

public:

	void AttachMO( IMouseObserver* _observer );
	void DetachMO( IMouseObserver* _observer );
	void NotifyMO( int _button, int _state, int _x, int _y, MouseNotification _type );

private:
	std::vector<IMouseObserver*> m_MoList;

};

#endif