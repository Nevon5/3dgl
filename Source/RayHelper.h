#pragma once
#include "stdafx.h"

struct Vertex
{
	glm::vec3	Position;		//float pad;
	int			MaterialId;
	glm::vec3	Normal;			
	int			TextureId;
	glm::vec2	TexCoord;		glm::vec2 pad2;
	//glm::vec4 Color;
	
	Vertex() {};
	Vertex(const glm::vec3& p, const int& mi, const glm::vec3& n, const int& ti, const glm::vec2& uv)	: Position(p), MaterialId(mi), Normal(n), TextureId(ti), TexCoord(uv) {};
};

struct Triangle
{
	Vertex Vertices[ 3 ];
};

struct Sphere
{
	glm::vec3	Center;
	float		Radius;
	glm::vec4	Color;
};

struct Ray
{
	glm::vec3	Origin;			
	bool NoBounce;
	glm::vec3	Direction;		int pad1;
};

//struct Intersection
//{
//	glm::vec3	Normal;
//	float		U;
//	glm::vec3	Position;
//	float		V;
//	int			TextureID;		glm::vec3 pad1;
//};

struct Intersection
{
	glm::vec3	Normal;		int pad1;
	glm::vec3	Position;	//int pad2;	
	int			PassNo;
	glm::vec4	Color;
	glm::vec4	AmbientDiffuse;
	glm::vec4	Specular;
};

struct PerFrame
{
	glm::vec3 CamPos;		float pad1;
	glm::vec3 CamLook;		float pad2;
	glm::vec3 CamUp;		float pad3;
	glm::vec3 CamRight;		float pad4;
};	