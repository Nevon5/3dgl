#include "ObjectManager.h"

ObjectManager::ObjectManager( )
{
}

ObjectManager::~ObjectManager( )
{
}

void ObjectManager::Init( GLuint _program )
{
	m_controlledUnit = ControlledUnit();
	m_controlledUnit.Init( glm::vec3( 0.0f, 5.0f, -4.0f ), glm::vec3( 0.0f, -0.75f, 0.32f ), 'w', 'a', 's', 'd', 'q', 'e');

	m_obj = ObjModel();
	std::string cubeTexture = m_obj.Load( "Content/OBJs/cube2.obj", 0 );

	m_obj.SaveToMesh( 1.0f, GetTextureId( cubeTexture ) );
	std::vector<Triangle> objTris = m_obj.GetTriangles();

	const int noOfTriangles = 2;
	const int noOfPointLights = 10;
	const int noOfSpheres = 1 + noOfPointLights;
	const glm::vec4 bright = glm::vec4( 1.0, 1.0, 1.0, 1.0 );
	const glm::vec4 middle = glm::vec4( 0.6, 0.6, 0.6, 1.0 );
	const glm::vec4 dark = glm::vec4( 0.1, 0.1, 0.1, 1.0 );
	const glm::vec4 none = glm::vec4( 0.0 );

	GLuint indexInShader = 0;
	m_triangleBuffer.Init( objTris.size() + noOfTriangles );
	m_triangleBuffer.Bind( _program, "TriangleBuffer", indexInShader );

	indexInShader = 1;
	m_sphereBuffer.Init( noOfSpheres );
	m_sphereBuffer.Bind( _program, "SphereBuffer", indexInShader );

	indexInShader = 4;
	m_pointLightBuffer.Init( noOfPointLights );
	m_pointLightBuffer.Bind( _program, "PointLightBuffer", indexInShader );

	PointLight pointLights[ noOfPointLights ];
	pointLights[ 0 ].Ambient = none;
	pointLights[ 0 ].Diffuse = middle;
	pointLights[ 0 ].Specular = dark;
	pointLights[ 0 ].Position = glm::vec3( 2.75, 1.0, 1.4 );
	pointLights[ 0 ].Range = 100.0f;
	pointLights[ 0 ].Att = glm::vec3(0.0, 0.0, 0.2);

	pointLights[ 1 ] = pointLights[ 0 ];
	pointLights[ 1 ].Position = glm::vec3( -2.75, 1.0, -1.4 );
	pointLights[ 2 ] = pointLights[ 0 ];
	pointLights[ 2 ].Position = glm::vec3( 2.75, -1.0, -1.4 );
	pointLights[ 3 ] = pointLights[ 0 ];
	pointLights[ 3 ].Position = glm::vec3( -2.75, 1.0, 1.4 );

	for ( size_t i = 4; i < noOfPointLights; i++ )
	{
		pointLights[ i ] = pointLights[ 0 ];
	}

	for ( size_t i = 0; i < noOfPointLights; i++ )
	{
		m_pointLightBuffer.Add( pointLights[ i ] );
	}
	m_pointLightBuffer.Update();


	// Create the floor
	Triangle triangles[ noOfTriangles ];
	int floorTextureId = GetTextureId( "Content/Textures/floor_512.png" );

	triangles[ 0 ].Vertices[ 0 ].Position = glm::vec3( +15.0, -2.0, -10.0 );
	triangles[ 0 ].Vertices[ 1 ].Position = glm::vec3( -5.0, -2.0, -10.0 );
	triangles[ 0 ].Vertices[ 2 ].Position = glm::vec3( -5.0, -2.0, +5.0 );
	triangles[ 0 ].Vertices[ 0 ].Normal = glm::vec3( 0.0, 1.0, 0.0 );
	triangles[ 0 ].Vertices[ 1 ].Normal = glm::vec3( 0.0, 1.0, 0.0 );
	triangles[ 0 ].Vertices[ 2 ].Normal = glm::vec3( 0.0, 1.0, 0.0 );
	triangles[ 0 ].Vertices[ 0 ].TexCoord = glm::vec2( 1.0, 1.0 );
	triangles[ 0 ].Vertices[ 1 ].TexCoord = glm::vec2( 0.0, 1.0 );
	triangles[ 0 ].Vertices[ 2 ].TexCoord = glm::vec2( 0.0, 0.0 );
	triangles[ 0 ].Vertices[ 0 ].TextureId = floorTextureId;
	
	triangles[ 1 ].Vertices[ 0 ].Position = glm::vec3( +15.0, -2.0, +5.0 );
	triangles[ 1 ].Vertices[ 1 ].Position = glm::vec3( +15.0, -2.0, -10.0 );
	triangles[ 1 ].Vertices[ 2 ].Position = glm::vec3( -5.0, -2.0, +5.0 );
	triangles[ 1 ].Vertices[ 0 ].Normal	= glm::vec3( 0.0, 1.0, 0.0 );
	triangles[ 1 ].Vertices[ 1 ].Normal	= glm::vec3( 0.0, 1.0, 0.0 );
	triangles[ 1 ].Vertices[ 2 ].Normal	= glm::vec3( 0.0, 1.0, 0.0 );
	triangles[ 1 ].Vertices[ 0 ].TexCoord = glm::vec2( 1.0, 0.0 );
	triangles[ 1 ].Vertices[ 1 ].TexCoord = glm::vec2( 1.0, 1.0 );
	triangles[ 1 ].Vertices[ 2 ].TexCoord = glm::vec2( 0.0, 0.0 );
	triangles[ 1 ].Vertices[ 0 ].TextureId = floorTextureId;

	for ( size_t i = 0; i < noOfTriangles; i++ )
	{
		m_triangleBuffer.Add( triangles[ i ] );
	}

	m_triangleBuffer.AddVector( objTris );
	m_triangleBuffer.Update();


	Sphere spheres[ noOfSpheres ];
	for ( size_t i = 0; i < noOfSpheres; i++ )
	{
		spheres[ i ].Center = glm::vec3( double( i ) - 1.0, +1.0, 1.0 );
		spheres[ i ].Radius = 0.5f;
		spheres[ i ].Color = glm::vec4( 0.4, 0.0, 0.4, 0.0 );
		m_sphereBuffer.Add( spheres[ i ] );
	}

	m_sphereBuffer.Update();
}

void ObjectManager::Update( float _dt )
{
	static float timePassed = 0.0f;
	static glm::vec3 centers[] = {	glm::vec3( 0.0, 1.5, 0.0 ), 
									glm::vec3( 2.0, 2.0, 2.0 ),
									glm::vec3( 0.0, 1.0, 2.0 ),
									glm::vec3( -2.0, 1.5, -2.0 ),
									glm::vec3( 1.0, 1.0, 0.0 ),
									glm::vec3(-1.0, 0.5, 0.0 ),
									glm::vec3( 3.0, 1.0, 0.0 ),
									glm::vec3( -2.0, 2.0, 1.0 ),
									glm::vec3( -5.0, 1.0, -2.0 ),
									glm::vec3( 0.0, 3.0, 0.0 ) };
	static float distFromCenter[] = { 5.0f, 1.0f, 2.0f, 1.5f, 1.2f, 2.0f, 1.7f, 1.9f, 2.0f, 1.0f };
	timePassed += _dt;

	m_controlledUnit.Update( _dt );
	
	for ( size_t i = 0; i < m_pointLightBuffer.GetElements().size(); i++ )
	{
		m_pointLightBuffer.GetElements()[ i ].Position.x = centers[ i ].x + distFromCenter[ i ] * cosf( timePassed * ( i - 5.5 ) / 2 );
		m_pointLightBuffer.GetElements()[ i ].Position.y = centers[ i ].y;
		m_pointLightBuffer.GetElements()[ i ].Position.z = centers[ i ].z + distFromCenter[ i ] * sinf( timePassed * (i-5.5)/2 );

		m_sphereBuffer.GetElements()[ i + 1 ].Radius = 0.1f;
		m_sphereBuffer.GetElements()[ i + 1 ].Center = m_pointLightBuffer.GetElements()[ i ].Position;
		m_sphereBuffer.GetElements()[ i + 1 ].Center.y += 0.18f;
	}
	m_pointLightBuffer.Update();
	m_sphereBuffer.Update();
}

void ObjectManager::Render( )
{

}

int ObjectManager::GetTextureId( std::string _textureName )
{
	static int textureId = 0;
	
	if ( m_textures.find( _textureName ) != m_textures.end() ) 
	{
		return m_textures[ _textureName ];
	}

	m_textures.insert( std::make_pair( _textureName, textureId ) );
	return textureId++;
}

//void ObjectManager::CreatePyramid( Triangle* _firstTriangle, const glm::vec3& _pos, float _sideSize )
//{
//	glm::vec3 bottom1 = _pos;
//	glm::vec3 bottom2 = glm::vec3( 0.5 + _pos.x, _pos.y, sqrt( 0.75 ) + _pos.z );
//	glm::vec3 bottom3 = glm::vec3( 1.0 + _pos.x, _pos.y, _pos.z );
//	glm::vec3 top = glm::vec3( 0.5 + _pos.x, sqrt(2.0/3.0) + _pos.y, (bottom1.z + bottom2.z + bottom3.z) / 3.0 );
//	
//	const glm::vec4 darkRed = glm::vec4( 0.4, 0.0, 0.0, 0.0 );
//	const glm::vec4 darkGreen = glm::vec4( 0.0, 0.4, 0.0, 0.0 );
//	const glm::vec4 darkBlue = glm::vec4( 0.0, 0.0, 0.4, 0.0 );
//
//	const glm::vec4 gray = glm::vec4( 0.4, 0.4, 0.4, 0.0 );
//	const glm::vec4 darkYellow = glm::vec4( 0.4, 0.4, 0.0, 0.0 );
//	const glm::vec4 darkPurple = glm::vec4( 0.4, 0.0, 0.4, 0.0 );
//	const glm::vec4 marineBlue = glm::vec4( 0.0, 0.4, 0.4, 0.0 );
//
//	const glm::vec2 bottomLeft = glm::vec2( 0.0, 1.0 );
//	const glm::vec2 bottomRight = glm::vec2( 1.0, 1.0 );
//	const glm::vec2 topT = glm::vec2( 0.5, 0.0 );
//
//	glm::vec3 normal(glm::cross( glm::vec3( bottom1 - bottom3 ), glm::vec3(bottom1-bottom2)));
//	_firstTriangle[ 0 ].Vertices[ 0 ].Position = bottom1;
//	_firstTriangle[ 0 ].Vertices[ 1 ].Position = bottom2;
//	_firstTriangle[ 0 ].Vertices[ 2 ].Position = bottom3;
//	_firstTriangle[ 0 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 0 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 0 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 0 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 0 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 0 ].Vertices[ 2 ].TexCoord = topT;
//	
//	normal = glm::vec3( glm::cross( glm::vec3( bottom1 - bottom2 ), glm::vec3( bottom1 - top ) ) );
//	_firstTriangle[ 1 ].Vertices[ 0 ].Position = bottom1;
//	_firstTriangle[ 1 ].Vertices[ 1 ].Position = bottom2;
//	_firstTriangle[ 1 ].Vertices[ 2 ].Position = top;
//	_firstTriangle[ 1 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 1 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 1 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 1 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 1 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 1 ].Vertices[ 2 ].TexCoord = topT;
//
//	normal = glm::vec3( glm::cross( glm::vec3( bottom2 - bottom3 ), glm::vec3( bottom2 - top ) ) );
//	_firstTriangle[ 2 ].Vertices[ 0 ].Position = bottom2;
//	_firstTriangle[ 2 ].Vertices[ 1 ].Position = bottom3;
//	_firstTriangle[ 2 ].Vertices[ 2 ].Position = top;
//	_firstTriangle[ 2 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 2 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 2 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 2 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 2 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 2 ].Vertices[ 2 ].TexCoord = topT;
//
//	normal = glm::vec3( glm::cross( glm::vec3( bottom3 - bottom1 ), glm::vec3( bottom3 - top ) ) );
//	_firstTriangle[ 3 ].Vertices[ 0 ].Position = bottom3;
//	_firstTriangle[ 3 ].Vertices[ 1 ].Position = bottom1;
//	_firstTriangle[ 3 ].Vertices[ 2 ].Position = top;
//	_firstTriangle[ 3 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 3 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 3 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 3 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 3 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 3 ].Vertices[ 2 ].TexCoord = topT;
//}
