#include "Input.h"

void Input::Init()
{
	g_inputInstance = this;

	for ( int i = 0; i < 256; i++ )
	{
		m_keysPressed[i] = false;
	}

	for ( int i = 0; i < 3; i++ )
	{
		m_mouseButtonsPressed[ i ] = false;
	}

	glutKeyboardFunc( KeyboardDownWrapper );
	glutKeyboardUpFunc( KeyboardUpWrapper );
	glutMouseFunc( MouseButtonWrapper );
	glutMotionFunc( MouseMoveWrapper );
	glutPassiveMotionFunc( MouseMovePassiveWrapper );
}

/* Called when a keyboard key has been pressed.*/
void Input::KeyboardDownCB( unsigned char _key, int _x, int _y )
{
	m_keysPressed[ _key ] = true;
	NotifyKO( _key, _x, _y, AKeyboard::KeyPressed );
	//printf( "%c down\n", _key );
}

/* Called when a keyboard key has been released.*/
void Input::KeyboardUpCB( unsigned char _key, int _x, int _y )
{
	m_keysPressed[ _key ] = false;
	NotifyKO( _key, _x, _y, AKeyboard::KeyReleased );
	//printf( "%c up\n", _key );
}

/* Called when a mouse button has been pressed.*/
void Input::MouseButtonCB( int _button, int _state, int _x, int _y )
{
	m_mouseButtonsPressed[ _button ] = !_state;
	//printf( "%s", m_mouseButtonsPressed[ _button ] ? "true" : "false" );
	NotifyMO( _button, _state, _x, _y, AMouse::MouseButton );
}

/* Called when the mouse is being moved while a button is pressed.*/
void Input::MouseMoveCB( int _x, int _y )
{
	m_mousePos = glm::ivec2( _x, _y );
	NotifyMO( 0, 0, _x, _y, AMouse::MouseMove );
	//printf( "Mouse at: %i, %i with pressed button.\n", _x, _y );
}

/* Called when the mouse is moved.*/
void Input::MouseMovePassiveCB( int _x, int _y )
{
	m_mousePos = glm::ivec2( _x, _y );
	NotifyMO( 0, 0, _x, _y, AMouse::MouseMovePassive );
	//printf( "Mouse at: %i, %i\n", _x, _y );
}

#pragma region Callback wrappers
void Input::KeyboardDownWrapper( unsigned char _key, int _x, int _y )
{
	g_inputInstance->KeyboardDownCB( _key, _x, _y );
}

void Input::KeyboardUpWrapper( unsigned char _key, int _x, int _y )
{
	g_inputInstance->KeyboardUpCB( _key, _x, _y );
}

void Input::MouseButtonWrapper( int _button, int _state, int _x, int _y )
{
	g_inputInstance->MouseButtonCB( _button, _state, _x, _y );
}

void Input::MouseMoveWrapper( int _x, int _y )
{
	g_inputInstance->MouseMoveCB( _x, _y );
}

void Input::MouseMovePassiveWrapper( int _x, int _y )
{
	g_inputInstance->MouseMovePassiveCB( _x, _y );
}
#pragma endregion
