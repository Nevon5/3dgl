#include "OpenGLTimer.h"

OpenGLTimer::OpenGLTimer(){}

OpenGLTimer::~OpenGLTimer(){}

void OpenGLTimer::Init()
{
	glGenQueries( 1, &m_startQuery );
	glGenQueries( 1, &m_endQuery );
}

void OpenGLTimer::Start()
{
	glQueryCounter( m_startQuery, GL_TIMESTAMP );
}

void OpenGLTimer::Stop()
{
	glQueryCounter( m_endQuery, GL_TIMESTAMP );
}

GLuint64 OpenGLTimer::GetTimeTaken( ) const
{
	GLint timeAvailable = 0;
	while ( !timeAvailable )
	{
		glGetQueryObjectiv( m_endQuery, GL_QUERY_RESULT, &timeAvailable );
	}

	GLuint64 startTime, endTime;
	GLuint64 elapsedTime;
	glGetQueryObjectui64v( m_startQuery, GL_QUERY_RESULT, &startTime );
	glGetQueryObjectui64v( m_endQuery, GL_QUERY_RESULT, &endTime );
	
	elapsedTime = ( endTime - startTime );// / 1000000.0;

	return elapsedTime;
}