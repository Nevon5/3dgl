#include "Timer.h"
#include <time.h>

Timer::Timer()
{
	m_deltaTime = -1.0f;
	m_resetTime = 0;
	m_timePaused = 0;
	m_previousTime = 0;
	m_currentTime = 0;
	m_stopTime = 0;
	m_secondsPerClockCycle = 1.0 / CLOCKS_PER_SEC;
}

Timer::~Timer()
{
}

void Timer::Reset()
{
	uintmax_t currentTime = clock();

	m_resetTime = currentTime;
	m_previousTime = currentTime;
	m_stopTime = 0;
	m_stopped = false;
}

/* If paused, start the timer and add the paused time.*/
void Timer::Start()
{
	if ( m_stopped )
	{
		uintmax_t startTime = clock( );
		m_timePaused += startTime - m_stopTime;

		m_previousTime = startTime;
		m_stopTime = 0;
		m_stopped = false;
	}
}

/* If not paused, stop the timer.*/
void Timer::Stop()
{
	if ( !m_stopped )
	{
		uintmax_t currentTime = clock();
		m_stopTime = currentTime;
		m_stopped = true;
	}
}

/* Calculate the time elapsed since last frame.*/
void Timer::Tick()
{
	if ( m_stopped )
	{
		m_deltaTime = 0.0f;
		return;
	}

	m_currentTime = clock();
	m_deltaTime = ( m_currentTime - m_previousTime ) * m_secondsPerClockCycle;
	m_previousTime = m_currentTime;

	/* Make sure the deltatime is not negative.*/
	if ( m_deltaTime < 0.0f )
	{
		m_deltaTime = 0.0f;
	}
}

float Timer::DeltaTime( ) const
{
	return m_deltaTime;
}

/* Get the total time elapsed since the last reset.*/
float Timer::TotalTime()
{
	if ( m_stopped )
	{
		return (( m_stopTime - m_timePaused ) - m_resetTime) * m_secondsPerClockCycle;
	}
	else
	{
		return (( m_currentTime - m_timePaused ) - m_resetTime) * m_secondsPerClockCycle;
	}
}