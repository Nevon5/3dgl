#ifndef Object_h
#define Object_h

#include "stdafx.h"
#include "RayHelper.h"

class Object
{
public:
	Object();
	~Object();

	virtual void Init();
	virtual void Update( float _dt );
	virtual void Render();

	const glm::vec3& GetPosition() { return m_position; };

protected:

	glm::vec3 m_position;
	glm::vec3 m_direction;
	std::vector<Triangle> m_triangles;
};

#endif