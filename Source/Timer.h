#ifndef TIMER_H
#define TIMER_H

#include "stdafx.h"

class Timer
{
public:
	Timer();
	~Timer();

	void Start();
	void Stop();
	void Tick();
	void Reset();

	float TotalTime();
	float DeltaTime( ) const;

private:

	/* The time (in clock cycles) when the timer was last reset.*/
	uintmax_t	m_resetTime;
	/* The amount of paused time (in clock cycles) accumulated.*/
	uintmax_t	m_timePaused;
	/* The time (in clock cycles) when the timer was stopped.*/
	uintmax_t	m_stopTime;
	/* The time (in clock cycles) when the current frame started.*/
	uintmax_t	m_currentTime;
	/* The time (in clock cycles) when the previous frame started.*/
	uintmax_t	m_previousTime;

	/* Time difference (in seconds) between the current and previous frame.*/
	float		m_deltaTime;
	double		m_secondsPerClockCycle;
	bool		m_stopped;
};


#endif