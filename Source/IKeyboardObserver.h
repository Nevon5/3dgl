#ifndef IKEYBOARDOBSERVER_H
#define IKEYBOARDOBSERVER_H

class IKeyboardObserver
{
public:
	virtual void KeyPressed( unsigned char _key, int _x, int _y ) = 0;
	virtual void KeyReleased( unsigned char _key, int _x, int _y ) = 0;
};

#endif