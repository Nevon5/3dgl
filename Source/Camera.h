#ifndef CAMERA_H
#define CAMERA_H

#include "stdafx.h"
#include "IUnitObserver.h"

class Camera : public IUnitObserver
{
public:
	Camera();
	~Camera();

	void Update	( float _dt );

	void Walk	( float _dist );
	void Strafe	( float _dist );
	void Ascend	( float _dist );

	void Pitch	( float _angle );
	void Yaw	( float _angle );
	void Roll	( float _angle );

	virtual void UnitMoved( glm::vec3 _pos, glm::vec3 _dir );

	void SetKeysPressedArrayPointer( const bool* const _keysPressed ) { m_keysPressed =  _keysPressed; };

	const glm::vec3& GetPosition()	{ return m_position; };
	const glm::vec3& GetLook()		{ return m_look; };
	const glm::vec3& GetUp()		{ return m_up; };
	const glm::vec3& GetRight( )	{ return m_right; };
	const float& GetFOV()			{ return m_fovFactor; };
	const float& GetFar( )			{ return m_farPlane; };

private:
	glm::vec3 m_position;
	glm::vec3 m_up;
	glm::vec3 m_right;
	glm::vec3 m_look;

	/* Changes the FOV, but is not the actual angle. The angle is arctan(m_fovMultiplier * 0.5) * 2. For example 2.0 gives a fov of 90 and 1.0 a fov of 53.  */
	float m_fovFactor;

	/* Array of 256 values representing ASCII chars. */
	const bool* m_keysPressed;

	float m_farPlane;
	float m_nearPlane;
};

#endif