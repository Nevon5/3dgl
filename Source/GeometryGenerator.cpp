//#include "GeometryGenerator.h"
//
//void GeometryGenerator::CreatePyramid( Triangle* _firstTriangle, const glm::vec3& _pos, float _sideSize )
//{
//	glm::vec3 bottom1 = _pos;
//	glm::vec3 bottom2 = glm::vec3( 0.5f + _pos.x, _pos.y, sqrt( 0.75f ) + _pos.z );
//	glm::vec3 bottom3 = glm::vec3( 1.0f + _pos.x, _pos.y, _pos.z );
//	glm::vec3 top = glm::vec3( 0.5f + _pos.x, sqrt(2.0f/3.0f) + _pos.y, (bottom1.z + bottom2.z + bottom3.z) / 3.0f );
//
//	const glm::vec2 bottomLeft = glm::vec2( 0.0f, 1.0f );
//	const glm::vec2 bottomRight = glm::vec2( 1.0f, 1.0f );
//	const glm::vec2 topT = glm::vec2( 0.5f, 0.0f );
//
//	//_firstTriangle[0].Vertices[0] = Vertex(glm::vec3(), 0, glm::vec3(), glm::vec2());
//	glm::vec3 normal(glm::cross( glm::vec3( bottom1 - bottom3 ), glm::vec3(bottom1-bottom2)));
//	_firstTriangle[ 0 ].Vertices[ 0 ].Position = bottom1;
//	_firstTriangle[ 0 ].Vertices[ 1 ].Position = bottom2;
//	_firstTriangle[ 0 ].Vertices[ 2 ].Position = bottom3;
//	_firstTriangle[ 0 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 0 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 0 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 0 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 0 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 0 ].Vertices[ 2 ].TexCoord = topT;
//	
//	normal = glm::vec3( glm::cross( glm::vec3( bottom1 - bottom2 ), glm::vec3( bottom1 - top ) ) );
//	_firstTriangle[ 1 ].Vertices[ 0 ].Position = bottom1;
//	_firstTriangle[ 1 ].Vertices[ 1 ].Position = bottom2;
//	_firstTriangle[ 1 ].Vertices[ 2 ].Position = top;
//	_firstTriangle[ 1 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 1 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 1 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 1 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 1 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 1 ].Vertices[ 2 ].TexCoord = topT;
//
//	normal = glm::vec3( glm::cross( glm::vec3( bottom2 - bottom3 ), glm::vec3( bottom2 - top ) ) );
//	_firstTriangle[ 2 ].Vertices[ 0 ].Position = bottom2;
//	_firstTriangle[ 2 ].Vertices[ 1 ].Position = bottom3;
//	_firstTriangle[ 2 ].Vertices[ 2 ].Position = top;
//	_firstTriangle[ 2 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 2 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 2 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 2 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 2 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 2 ].Vertices[ 2 ].TexCoord = topT;
//
//	normal = glm::vec3( glm::cross( glm::vec3( bottom3 - bottom1 ), glm::vec3( bottom3 - top ) ) );
//	_firstTriangle[ 3 ].Vertices[ 0 ].Position = bottom3;
//	_firstTriangle[ 3 ].Vertices[ 1 ].Position = bottom1;
//	_firstTriangle[ 3 ].Vertices[ 2 ].Position = top;
//	_firstTriangle[ 3 ].Vertices[ 0 ].Normal = normal;
//	_firstTriangle[ 3 ].Vertices[ 1 ].Normal = normal;
//	_firstTriangle[ 3 ].Vertices[ 2 ].Normal = normal;
//	_firstTriangle[ 3 ].Vertices[ 0 ].TexCoord = bottomLeft;
//	_firstTriangle[ 3 ].Vertices[ 1 ].TexCoord = bottomRight;
//	_firstTriangle[ 3 ].Vertices[ 2 ].TexCoord = topT;
//}
