#include <GL/glew.h>
#include <GL/glut.h>
#include <glm.hpp>

#include <stdio.h>
#include <iostream>
#include <vector>

#define SAFE_DELETE(x) if(x) { delete(x); (x) = NULL; }