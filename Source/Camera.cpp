#include "Camera.h"
#include <gtx/rotate_vector.hpp>

Camera::Camera( )
{
	m_position	= glm::vec3( 0.0f, 0.0f, 0.0f );
	m_look		= glm::vec3( 0.0f, 0.0f, 1.0f );
	m_up		= glm::vec3( 0.0f, 1.0f, 0.0f );
	//m_position	= glm::vec3( 0.8f, 10.0f, -10.0f );
	//m_look		= glm::vec3( 0.0f, -0.7f, 0.7f );
	//m_up		= glm::vec3( 0.0f, 0.7f, 0.7f );
	//m_position		= glm::vec3( 0.0f, 0.0f, 0.0f );
	//m_look			= glm::vec3( 0.0f, 0.0f, 1.0f );
	//m_up			= glm::vec3( 0.0f, 1.0f, 0.0f );
	m_right			= glm::vec3( 1.0f, 0.0f, 0.0f );
	m_fovFactor		= 1.0f;
	m_nearPlane		= 1.0f;
	m_farPlane		= 1000.0f;
}

Camera::~Camera( ){}

void Camera::Update( float _dt )
{
	//if ( m_keysPressed['w'] == true )
	//{
	//	printf( "w key down" );
	//}
	//else
	//{
	//	printf( "w key up" );
	//}
}

/* Move the camera along the Look vector.*/
void Camera::Walk( float _dist )
{
	m_position += _dist * m_look;
}

/* Move the camera along the Right vector.*/
void Camera::Strafe( float _dist )
{
	m_position += _dist * m_right;
}

/* Move the camera along the Up vector.*/
void Camera::Ascend( float _dist )
{
	m_position += _dist * m_up;
}

/* Rotate around the camera's Up vector (radians).*/
void Camera::Pitch( float _angle )
{
	m_look = glm::rotate( m_look, _angle, m_up );
	m_right = glm::rotate( m_right, _angle, m_up );
}

/* Rotate around the camera's Right vector (radians).*/
void Camera::Yaw( float _angle )
{
	m_look = glm::rotate( m_look, _angle, m_right );
	m_up = glm::rotate( m_up, _angle, m_right );
}

/* Rotate around the camera's Look vector (radians).*/
void Camera::Roll( float _angle )
{
	m_right = glm::rotate( m_right, _angle, m_look );
	m_up = glm::rotate( m_up, _angle, m_look );
}

void Camera::UnitMoved( glm::vec3 _pos, glm::vec3 _dir )
{
	m_position = _pos;
	m_look = _dir;
	m_right = glm::normalize( glm::cross( glm::vec3( 0, 1, 0 ), m_look ) );
	m_up = glm::normalize( glm::cross( m_look, m_right ) );
}