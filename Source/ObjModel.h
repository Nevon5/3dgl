#pragma once

#include "stdafx.h"
#include "RayHelper.h"
#include "LightHelper.h"
#include <map>
#include <fstream>

class ObjModel
{
	struct ObjMaterial
	{
		int			IlluminationModel;
		glm::vec4	Ambient;
		glm::vec4	Diffuse;
		glm::vec4	Specular;
		glm::vec3	TransmissionFilter;
		std::string	TextureMap;
		float		OpticalDensity;

		ObjMaterial()
		{
			IlluminationModel = 1;
			Ambient = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			Diffuse = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			Specular = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			TransmissionFilter = glm::vec3(1.0f, 1.0f, 1.0f);
			TextureMap = "";
			OpticalDensity = 1.0f;
		}
	};

	//struct Group
	//{

	//};

	//struct MeshData
	//{
	//	std::vector<Vertex> Vertices;
	//	std::vector<unsigned int> Indices;
	//};

	typedef std::map<std::string, unsigned int> MaterialId_Map;
	//typedef std::map<std::string, ObjMaterial>	Material_Map;
	//typedef std::map<std::string, Group>	Group_Map;

public:
	ObjModel();
	~ObjModel();
	std::string Load(char* _filename, unsigned int _startingMatId );
	//MeshData GetMesh();
	//void SaveToMesh(float p_SizeRatio);
	void SaveToMesh(float p_SizeRatio, int p_textureId);
	const std::vector<Triangle>& GetTriangles() { return m_triangles; };
	const std::vector<Material>& GetMaterials() { return m_materials; };
	
	//Material_Map const GetMaterials() { return m_materials; };
	
private:
	void ParseGroup(std::ifstream& f);
	void ParseComment(std::ifstream& f);
	void ParseMaterial(std::ifstream& f);
	void ParseFace(std::ifstream& f);
	void ParsePosition(std::ifstream& f);
	void ParseNormal(std::ifstream& f);
	void ParseTexCoord(std::ifstream& f);

	std::string ParseMaterialFile(std::ifstream& f, std::string fileDir);

	//int CompareIndices(int i, int j);

	glm::vec3&	GetPosition(unsigned int _index);
	glm::vec2&	GetTexCoord(unsigned int _index);
	glm::vec3&	GetNormal(unsigned int _index);
	int			GetMaterialId(unsigned int _index);

private:
	//Group_Map				m_groups;
	//Material_Map			m_materials;
	MaterialId_Map			m_materialIds;
	std::vector<Material>	m_materials;
	std::vector<glm::vec3>	m_positions;
	std::vector<glm::vec3>	m_normals;
	std::vector<glm::vec2>	m_texCoords;
	std::vector<glm::vec4>	m_indices;

	unsigned int m_startingMatId;
	unsigned int m_currentMatId;
	unsigned int m_textureId;
	//Material				m_currentMat;

	//MeshData m_Mesh;
	std::vector<Triangle>	m_triangles;
};