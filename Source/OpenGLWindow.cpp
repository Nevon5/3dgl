#include "OpenGLWindow.h"

OpenGLWindow::OpenGLWindow()
{
	m_winPos = glm::ivec2( 10, 10 );
	m_winSize = glm::ivec2( 758, 758 );

	//m_winPos = glm::ivec2( 1150, 200 );
	//m_winSize = glm::ivec2( 1280, 720 );
	//m_winSize = glm::ivec2( 1536, 880 );
	m_winName = "Raytracing";
	m_maxFPS = 60;
	m_backgroundColor = glm::vec4( 0.0f, 0.0f, 0.0f, 0.0f );
}

OpenGLWindow::~OpenGLWindow()
{
}


void OpenGLWindow::Init( int* _argc, char* _argv[ ] )
{
	g_instance = this;
	InitGlutWindow( _argc, _argv );
	InitGlew( );

	m_timer = Timer();
	m_input = Input();
	m_input.Init();
	m_application = Application();
	m_application.Init(m_winSize);

	m_application.SetKeysPressedArrayPointer(m_input.GetKeysPressedArray());
	m_application.SetMouseButtonsPressedArrayPointer( m_input.GetMouseButtonsPressedArray() );
	m_application.SetMousePosPointer( m_input.GetMousePos() );
	
	glutDisplayFunc( RenderWrapper );

	if ( 0 < m_maxFPS )
	{
		glutTimerFunc( 1000 / m_maxFPS, TimerWrapper, 0 );
	}
	else
	{
		glutIdleFunc(IdleWrapper);
	}

	m_timer.Start( );

	glClearColor( m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, m_backgroundColor.w );
	glutMainLoop();
}

void OpenGLWindow::InitGlutWindow( int* _argc, char* _argv[ ] )
{
	glutInit( _argc, _argv );
	/* Enable double buffering and the color buffer to render to.*/
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA );
	glutInitWindowSize( m_winSize.x, m_winSize.y );
	glutInitWindowPosition( m_winPos.x, m_winPos.y );
	glutCreateWindow( m_winName );
}

void OpenGLWindow::InitGlew()
{
	GLenum result = glewInit( );
	if ( result != GLEW_OK )
	{
		printf( "Failed to init Glew: '%s'\n", glewGetErrorString( result ) );
	}
}

void OpenGLWindow::RenderCB()
{
	static float time = 0.0f;
	static int frames = 0;

	m_timer.Tick( );

	float dt = m_timer.DeltaTime();
	time += dt;
	frames++;
	//printf( "DeltaTime: %f\n", dt );
	
	if ( 1.0f < time )
	{
		char name[30];
		sprintf_s(name, sizeof(name), "DT: %f FPS: %f", time / frames, float(frames) / time );
		glutSetWindowTitle(name);
		time = 0.0f;
		frames = 0;
	}

	m_application.Update( dt );
	m_application.Render();
}

void OpenGLWindow::TimerCB( int _value )
{
	glutPostRedisplay( );
	glutTimerFunc( 1000 / m_maxFPS, TimerWrapper, 0 );
}

void OpenGLWindow::IdleCB()
{
	glutPostRedisplay();
}

#pragma region Callback Wrappers
void OpenGLWindow::RenderWrapper()
{
	g_instance->RenderCB( );
}

void OpenGLWindow::TimerWrapper( int _value )
{
	g_instance->TimerCB( _value );
}

void OpenGLWindow::IdleWrapper()
{
	g_instance->IdleCB();
}
#pragma endregion
