#include "ControlledUnit.h"
#include <gtx/rotate_vector.hpp>

ControlledUnit::ControlledUnit( )
{
	m_moved = true;
}

ControlledUnit::~ControlledUnit( )
{
}

void ControlledUnit::Init( glm::vec3 _pos, glm::vec3 _look, char _forward, char _left, char _backward, char _right, char _descend, char _ascend )
{
	m_position = _pos;
	m_direction = _look;
	m_controlKeys[ 0 ] = _forward;
	m_controlKeys[ 1 ] = _left;
	m_controlKeys[ 2 ] = _backward;
	m_controlKeys[ 3 ] = _right;
	m_controlKeys[ 4 ] = _descend;
	m_controlKeys[ 5 ] = _ascend;
}

void ControlledUnit::Update( float _dt )
{
	MoveWithKeys();
	MoveWithMouse();

	if ( m_moved )
	{
		m_moved = false;
		NotifyUO( m_position, m_direction );
	}
}

void ControlledUnit::Render( )
{

}

void ControlledUnit::MoveWithKeys( )
{
	/* Exit when escape key is pressed.*/
	if ( m_keysPressed[ 27 ] )
	{
		exit(0);
	}
	if (	m_keysPressed[ m_controlKeys[ 0 ] ] || 
			m_keysPressed[ m_controlKeys[ 0 ] +  32] )
	{
		m_position += m_direction * m_speeds.Forward;
		m_moved = true;
	}
	if (	m_keysPressed[ m_controlKeys[ 2 ] ] ||
			m_keysPressed[ m_controlKeys[ 2 ] + 32 ] )
	{
		m_position -= m_direction * m_speeds.Backward;
		m_moved = true;
	}
	if (	m_keysPressed[ m_controlKeys[ 1 ] ] ||
			m_keysPressed[ m_controlKeys[ 1 ] + 32 ] )
	{
		m_position -= glm::cross( glm::vec3( 0.0f, 1.0f, 0.0f ), m_direction ) * m_speeds.Strafe;
		m_moved = true;
	}
	if (	m_keysPressed[ m_controlKeys[ 3 ] ] ||
			m_keysPressed[ m_controlKeys[ 3 ] + 32 ] )
	{
		m_position += glm::cross( glm::vec3( 0.0f, 1.0f, 0.0f ), m_direction ) * m_speeds.Strafe;
		m_moved = true;
	}
	if (	m_keysPressed[ m_controlKeys[ 5 ] ] ||
			m_keysPressed[ m_controlKeys[ 5 ] + 32 ] )
	{
		m_position += glm::vec3( 0.0f, m_speeds.Fly, 0.0f );
		m_moved = true;
	}
	if (	m_keysPressed[ m_controlKeys[ 4 ] ] ||
			m_keysPressed[ m_controlKeys[ 4 ] + 32 ] )
	{
		m_position -= glm::vec3( 0.0f, m_speeds.Fly, 0.0f );
		m_moved = true;
	}

	//printf("%.3f, %.3f, %.3f\n", m_position.x, m_position.y, m_position.z);
}

void ControlledUnit::MoveWithMouse()
{
	/* If the right mouse button is pressed and the mouse position has changed, rotate the unit.*/
	if ( m_mouseButtonsPressed[2] && m_prevMousePos != *m_mousePos )
	{
		int x = m_mousePos->x - m_prevMousePos.x;
		int y = m_mousePos->y - m_prevMousePos.y;

		float xAngle = (float) x * m_speeds.Rotation;
		float yAngle = (float) y * m_speeds.Rotation;

		glm::vec3 up = glm::vec3( 0, 1, 0 );
		glm::vec3 right = glm::cross( up, m_direction );
		glm::vec3 newDir = m_direction;

		newDir = glm::rotate( newDir, yAngle, right );

		/* Prevent the unit from looking straight up, making the cross product of up and direction safe.*/
// TODO: This does not work.
		if ( newDir != up )
		{
			m_direction = newDir;
		}

		m_direction = glm::normalize(glm::rotate( m_direction, xAngle, up ));

		m_moved = true;
	}
	m_prevMousePos = *m_mousePos;
}