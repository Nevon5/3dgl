#ifndef ControlledUnit_h
#define ControlledUnit_h

#include "stdafx.h"
#include "Object.h"
#include "AUnit.h"
#include "IMouseObserver.h"
#include <map>

class ControlledUnit : public Object, public AUnit
{
public:
	enum KeyBinding
	{
		MoveForward,
		MoveBackward,
		StrafeLeft,
		StrafeRight,
		MoveUp,
		MoveDown,
		SizeOfEnum
	};

	struct Speeds
	{
		float Forward = 0.05f;
		float Backward = 0.05f;//0.03f;
		float Strafe = 0.05f;//0.03f;
		float Fly = 0.05f;
		float Rotation = 0.004f;
	};

	ControlledUnit( );
	~ControlledUnit( );

	void Init( glm::vec3 _pos, glm::vec3 _look, char _forward, char _left, char _backward, char _right, char _descend, char _ascend );
	void Update( float _dt );
	void Render();

	void SetKeysPressedArrayPointer( const bool* const _keysPressed ) { m_keysPressed = _keysPressed; };
	void SetMouseButtonsPressedArrayPointer( const bool* const _buttonsPressed ) { m_mouseButtonsPressed = _buttonsPressed; };
	void SetMousePosPointer( const glm::ivec2* const _mousePos ) { m_mousePos = _mousePos; };

private:
	void MoveWithKeys( );
	void MoveWithMouse( );

private:
	Speeds m_speeds;

	const bool* m_keysPressed;
	const bool* m_mouseButtonsPressed;

	bool m_moved;
	const glm::ivec2* m_mousePos;
	glm::ivec2 m_prevMousePos;

	glm::vec3 m_up;
	glm::vec3 m_right;

	char m_controlKeys[6];
};

#endif