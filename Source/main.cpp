#include "stdafx.h"
#include "OpenGLWindow.h"

int main( int argc, char* argv[] )
{
	OpenGLWindow window = OpenGLWindow();

	window.Init( &argc, argv );

	return 0;
}