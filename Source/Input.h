#ifndef INPUT_H
#define INPUT_H

#include "stdafx.h"
#include "AKeyboard.h"
#include "AMouse.h"

class Input : public AKeyboard, public AMouse
{
public:
	void Init( );
	void Update( float _dt );
	const bool* const GetKeysPressedArray( ) { return m_keysPressed; };
	const bool* const GetMouseButtonsPressedArray( ) { return m_mouseButtonsPressed; };
	const glm::ivec2* const GetMousePos( ) { return &m_mousePos; };

	void KeyboardDownCB( unsigned char _key, int _x, int _y );
	void KeyboardUpCB( unsigned char _key, int _x, int _y );
	void MouseButtonCB( int _button, int _state, int _x, int _y );
	void MouseMoveCB( int _x, int _y );
	void MouseMovePassiveCB( int _x, int _y );

	static void KeyboardDownWrapper( unsigned char _key, int _x, int _y );
	static void KeyboardUpWrapper( unsigned char _key, int _x, int _y );
	static void MouseButtonWrapper( int _button, int _state, int _x, int _y );
	static void MouseMoveWrapper( int _x, int _y );
	static void MouseMovePassiveWrapper( int _x, int _y );
	
private:
	bool m_keysPressed[ 256 ];
	bool m_mouseButtonsPressed[ 3 ];
	glm::ivec2 m_mousePos;
};

static Input* g_inputInstance;

#endif