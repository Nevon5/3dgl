#include "AUnit.h"

void AUnit::AttachUO( IUnitObserver* _observer )
{
	m_UoList.push_back( _observer );
}

void AUnit::DetachUO( IUnitObserver* _observer )
{
	if ( m_UoList.size( ) < 1 )
	{
		return;
	}

	std::vector<IUnitObserver*>::iterator i = m_UoList.begin( );

	for ( ; i != m_UoList.end( ); i++ )
	{
		if ( *i == _observer )
		{
			m_UoList.erase( i );
			return;
		}
	}
}

void AUnit::NotifyUO( glm::vec3 _pos, glm::vec3 _dir )
{
	if ( m_UoList.size( ) < 1 )
	{
		return;
	}

	for ( int i = 0; i < m_UoList.size( ); ++i )
	{
		// If the observer exist, notify that a key has been pressed/released.
		if ( m_UoList[ i ] != 0 )
		{
			m_UoList[ i ]->UnitMoved( _pos, _dir );
		}
	}
}