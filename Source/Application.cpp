#include "Application.h"
#include <fstream>
#include <SOIL.h>
#include <sstream>

Application::Application() 
{
	m_noOfPasses = 3;
}

Application::~Application()
{
}

void Application::Init( glm::ivec2 _winSize )
{
	m_workGroups.x = _winSize.x / 8;
	m_workGroups.y = _winSize.y / 8;

	CompileShaders();
	GenerateTexture( _winSize );
	glUseProgram( m_primaryRaysProgram );

	m_camera = Camera();
	m_timer = OpenGLTimer();
	m_timer.Init();
	m_objectManager.Init( m_primaryRaysProgram );

	m_objectManager.GetControlledUnit()->AttachUO( (IUnitObserver*) &m_camera );
	
	CreateFullscreenQuad();

	glActiveTexture( GL_TEXTURE0 );
	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, m_computeTexture );

	float fov = m_camera.GetFOV();
	glProgramUniform2f( m_primaryRaysProgram, glGetUniformLocation( m_primaryRaysProgram, "g_fovFactor" ), fov, fov * float( _winSize.y ) / float( _winSize.x ) );
	glProgramUniform1i( m_primaryRaysProgram, glGetUniformLocation( m_primaryRaysProgram, "g_destTex" ), 0 );
	glProgramUniform2i( m_primaryRaysProgram, glGetUniformLocation( m_primaryRaysProgram, "g_winSize" ), _winSize.x, _winSize.y );
	glProgramUniform2i( m_intersectionProgram, glGetUniformLocation( m_intersectionProgram, "g_winSize" ), _winSize.x, _winSize.y );
	glProgramUniform1f( m_intersectionProgram, glGetUniformLocation( m_intersectionProgram, "g_farPlane" ), m_camera.GetFar() );
	glProgramUniform2i( m_colorProgram, glGetUniformLocation( m_colorProgram, "g_winSize" ), _winSize.x, _winSize.y );
	glProgramUniform1i( m_colorProgram, glGetUniformLocation( m_colorProgram, "g_noOfPasses" ), m_noOfPasses );

	GLuint indexInShader = 2;
	m_rayBuffer.Init( _winSize.x * _winSize.y );
	m_rayBuffer.Bind( m_primaryRaysProgram, "RayBuffer", indexInShader );
	indexInShader = 3;
	m_intersectBuffer.Init( _winSize.x * _winSize.y );
	m_intersectBuffer.Bind( m_intersectionProgram, "IntersectionBuffer", indexInShader );

	m_perFrame.CamPos	 = m_camera.GetPosition();
	m_perFrame.CamLook	 = m_camera.GetLook();
	m_perFrame.CamUp	 = m_camera.GetUp();
	m_perFrame.CamRight	 = m_camera.GetRight();

	indexInShader = 0;
	m_bufferPerFrame.Init( 1 );
	m_bufferPerFrame.Bind( m_primaryRaysProgram, "BufferPerFrame", indexInShader );
	m_bufferPerFrame.Update( &m_perFrame, 1 );

	glUseProgram( m_colorProgram );
	
	GLuint texHandle;
	glGenTextures( 1, &texHandle );

	glActiveTexture( GL_TEXTURE2 );
	glBindTexture( GL_TEXTURE_2D_ARRAY, texHandle );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	LoadTextures();
	
	glUseProgram( m_renderProgram );

	/* Sets the texture to use in the renderprogram, where textureToUse is the slot in which the texture lies. (Slot is set in glActiveTexture() )*/
	GLuint textureToUse = 0;
	glUniform1i( glGetUniformLocation( m_renderProgram, "srcTex" ), textureToUse ); 
}

void Application::LoadTextures() 
{
	int w, h, c;
	std::map<std::string, unsigned int> textures = m_objectManager.GetTextures();
	std::map<std::string, unsigned int>::iterator it = textures.begin();
	unsigned char* pixels;
	int mipmaps = 1;

	while ( it != textures.end() )
	{
		pixels = SOIL_load_image( it->first.c_str(), &w, &h, &c, SOIL_LOAD_RGB );
		glTexStorage3D( GL_TEXTURE_2D_ARRAY, mipmaps, GL_RGB32F, w, h, 10 );
		glTexSubImage3D( GL_TEXTURE_2D_ARRAY, 0, 0, 0, it->second, w, h, 1, GL_RGB, GL_UNSIGNED_BYTE, pixels );
		it++;
	}
}

void Application::Update( float _dt )
{
	glClearTexImage( m_computeTexture, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
	m_camera.Update( _dt );
	m_objectManager.Update( _dt );
}

void Application::Render()
{
	glClear( GL_COLOR_BUFFER_BIT );

	m_objectManager.Render(); 
	Raytrace();
	RenderComputeTexture();

	glutSwapBuffers( );
}

void Application::Raytrace()
{
	static GLuint64 primaryRaysTime = 0;
	static GLuint64 intersectTime = 0;
	static GLuint64 colorTime = 0;
	static GLuint64 totalTime = 0;
	static int frames = 0;

	//static GLuint64 primaryRaysTot = 0;
	//static GLuint64 intersectTot = 0;
	//static GLuint64 colorTot = 0;
	//static int framesTot = 0;

	m_perFrame.CamPos	= m_camera.GetPosition();
	m_perFrame.CamLook	= m_camera.GetLook();
	m_perFrame.CamUp	= m_camera.GetUp();
	m_perFrame.CamRight = m_camera.GetRight();
	m_bufferPerFrame.Update( &m_perFrame, 1 );
	
	m_timer.Start();
	glUseProgram( m_primaryRaysProgram );
	glDispatchCompute( m_workGroups.x, m_workGroups.y, 1 );
	m_timer.Stop();
	primaryRaysTime += m_timer.GetTimeTaken();
	
	for ( int i = 0; i < m_noOfPasses; i++ )
	{
		glProgramUniform1i( m_intersectionProgram, glGetUniformLocation( m_intersectionProgram, "g_passNo" ), i + 1 ); 
		glProgramUniform1i( m_colorProgram, glGetUniformLocation( m_colorProgram, "g_passNo" ), i + 1 );

		m_timer.Start();
		glUseProgram( m_intersectionProgram );
		glDispatchCompute( m_workGroups.x, m_workGroups.y, 1 );
		m_timer.Stop();
		intersectTime += m_timer.GetTimeTaken();

		m_timer.Start();
		glUseProgram( m_colorProgram );
		glDispatchCompute( m_workGroups.x, m_workGroups.y, 1 );
		m_timer.Stop();
		colorTime += m_timer.GetTimeTaken();
	}

	totalTime = primaryRaysTime + intersectTime + colorTime;
	frames++;


	if ( 1000000000 < totalTime )
	{
		printf( "Primary Rays: %f\tIntersection: %f\tColor: %f\nAvg per pass: \t\tIntersection: %f\tColor: %f\n\n", 
			float( primaryRaysTime ) / float( frames * 1000000 ), 
			float( intersectTime ) / float( frames * 1000000 ),
			float( colorTime ) / float( frames * 1000000 ),
			float( intersectTime ) / float( m_noOfPasses *  frames * 1000000 ),
			float( colorTime ) / float( m_noOfPasses *  frames * 1000000 ) );
		//primaryRaysTot += primaryRaysTime;
		//intersectTot += intersectTime;
		//colorTot += colorTime;
		//framesTot += frames;
		//printf( "Totals:\nPrimary Rays: %f\tIntersection: %f\tColor: %f\nAvg per pass: \t\tIntersection: %f\tColor: %f\n\n\n",
		//	float( primaryRaysTot ) / float( framesTot * 1000000 ),
		//	float( intersectTot ) / float( framesTot * 1000000 ),
		//	float( colorTot ) / float( framesTot * 1000000 ),
		//	float( intersectTot ) / float( m_noOfPasses *  framesTot * 1000000 ),
		//	float( colorTot ) / float( m_noOfPasses *  framesTot * 1000000 ) );
		primaryRaysTime = 0; 
		intersectTime = 0;
		colorTime = 0;
		totalTime = 0;
		frames = 0;
	}
	
	glUseProgram( m_renderProgram );
}

void Application::RenderComputeTexture()
{
	glEnableVertexAttribArray( 0 );
	/* Bind the buffer again to prepare for the draw call.*/
	glBindBuffer( GL_ARRAY_BUFFER, m_renderVBO );

	int indexOfAttribute = 0;
	int componentsInAttribute = 3;
	int normaliceAttribute = GL_FALSE;
	glVertexAttribPointer( indexOfAttribute, componentsInAttribute, GL_FLOAT, normaliceAttribute, 0, 0 );
	
	int indexOfFirstVertex = 0;
	int noOfVertices = 4;
	/* Ordered draw so there is no index buffer.*/
	glDrawArrays( GL_QUADS, indexOfFirstVertex, noOfVertices );

	/* It is good practice to disable each vertex attribute when it is not immediately used. Leaving it enabled when a shader is not using it is a sure way of asking for trouble. */
	glDisableVertexAttribArray( 0 );
}

void Application::CompileShaders( )
{
	CreateProgram( &m_renderProgram );
	CreateProgram( &m_primaryRaysProgram );
	CreateProgram( &m_intersectionProgram );
	CreateProgram( &m_colorProgram );
	
	const char* includePaths[2];

	includePaths[ 0 ] = "Content/Shaders/RayHelper.glsl";
	includePaths[ 1 ] = "Content/Shaders/LightHelper.glsl";

	AddShader( m_renderProgram, "Content/Shaders/VertexShader.glsl", GL_VERTEX_SHADER );
	AddShader( m_renderProgram, "Content/Shaders/FragmentShader.glsl", GL_FRAGMENT_SHADER );
	AddShader( m_primaryRaysProgram, "Content/Shaders/PrimaryRaysStage.glsl", GL_COMPUTE_SHADER, includePaths, 2 );
	AddShader( m_intersectionProgram, "Content/Shaders/IntersectionStage.glsl", GL_COMPUTE_SHADER, includePaths, 2 );
	AddShader( m_colorProgram, "Content/Shaders/ColorStage.glsl", GL_COMPUTE_SHADER, includePaths, 2 );

	CheckProgram( m_renderProgram );
	CheckProgram( m_primaryRaysProgram );
	CheckProgram( m_intersectionProgram );
	CheckProgram( m_colorProgram );
}

void Application::AddShader( GLuint _shaderProgram, const char* _shaderPath, GLenum _shaderType, const char** _includePaths /* = nullptr */, int _nrOfIncludes /* = 0 */ )
{
	std::string shaderCode;
	std::string shaderSizes;
	int rows = 0;

	for ( int i = 0; i < _nrOfIncludes; i++ )
	{
		shaderCode += LoadFileToString( _includePaths[i], &rows );
		shaderSizes += std::to_string(rows) + ", ";
	}

	shaderCode += LoadFileToString( _shaderPath, &rows );
	shaderSizes += std::to_string( rows ) + ", ";

	GLuint shaderObject = glCreateShader( _shaderType );
	if ( shaderObject == 0 )
	{
		printf( "Error creating shader %s. Type %d\n", _shaderPath, _shaderType );
		return;
	}

	const char* source = shaderCode.c_str( );
	glShaderSource( shaderObject, 1, &source, NULL );
	glCompileShader( shaderObject );

	GLint success;
	glGetShaderiv( shaderObject, GL_COMPILE_STATUS, &success );
	if ( !success )
	{
		GLchar infoLog[ 1024 ];
		glGetShaderInfoLog( shaderObject, sizeof( infoLog ), NULL, infoLog );
		printf( "Error compiling shader %s. \nType %d \nSizes: %s \n\n%s\n\n", _shaderPath, _shaderType, shaderSizes.c_str(), infoLog );
		return;
	}

	glAttachShader( _shaderProgram, shaderObject );
}

std::string Application::LoadFileToString( const char* _shaderPath, int* _noOfRows )
{
	std::string code;
	std::ifstream file;
	file.open( _shaderPath );
	if ( !file.is_open() )
	{
		printf( "Error could not find shader file %s\n", _shaderPath );
		return code;
	}

	int noOfRows = 0;
	char line[ 256 ];
	while ( !file.eof() )
	{
		memcpy( line, "", 256 );
		file.getline( line, 256 );
		code += line;
		code += '\n';
		noOfRows++;
	}
	file.close();
	*_noOfRows += noOfRows;

	return code;
}

void Application::CreateProgram( GLuint* _program )
{
	*_program = glCreateProgram();
	if ( *_program == 0 )
	{
		printf( "Error creating shader program." );
	}
}

void Application::CheckProgram( GLuint _program )
{
	GLint success;
	GLchar errorLog[ 1024 ] = { 0 };
	glLinkProgram( _program );
	glGetProgramiv( _program, GL_LINK_STATUS, &success );
	if ( success == 0 )
	{
		glGetProgramInfoLog( _program, sizeof( errorLog ), NULL, errorLog );
		printf( "Error linking shader program: '%s'\n", errorLog );
		return;
	}

	/* TODO: Only during development.*/
	glValidateProgram( _program );
	glGetProgramiv( _program, GL_VALIDATE_STATUS, &success );
	if ( success == 0 )
	{
		glGetProgramInfoLog( _program, sizeof( errorLog ), NULL, errorLog );
		printf( "Invalid shader program: '%s'\n", errorLog );
		return;
	}
}

void Application::GenerateTexture( glm::ivec2 _size )
{
	GLuint texHandle;
	glGenTextures( 1, &texHandle );

	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texHandle );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA32F, _size.x, _size.y, 0, GL_RGBA, GL_FLOAT, NULL );

	GLenum error1 = glGetError( );
	if ( error1 != GL_NO_ERROR )
	{
		printf( "Error generating texture. \n'%s'", gluErrorString( error1 ) );
	}

	/* Bind it to a image unit as it is used as an image (writing to it).*/
	glBindImageTexture( 0, texHandle, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F );

	GLenum error = glGetError( );
	if ( error != GL_NO_ERROR )
	{
		printf( "Error generating texture. \n'%s'", gluErrorString( error ) );
	}

	m_computeTexture = texHandle;
}

void Application::CreateFullscreenQuad()
{
	glm::vec3 vertices[ 4 ];
	vertices[ 0 ] = glm::vec3( -1.0f, -1.0f, 0.0f );
	vertices[ 1 ] = glm::vec3( -1.0f, +1.0f, 0.0f );
	vertices[ 2 ] = glm::vec3( +1.0f, +1.0f, 0.0f );
	vertices[ 3 ] = glm::vec3( +1.0f, -1.0f, 0.0f );

	/* Creates "1" object.*/
	glGenBuffers( 1, &m_renderVBO );
	/* Bind the buffer object. Specify what the buffer should be used for.*/
	glBindBuffer( GL_ARRAY_BUFFER, m_renderVBO );
	/* Fill the object with data.*/
	glBufferData( GL_ARRAY_BUFFER, sizeof( vertices ), vertices, GL_STATIC_DRAW );
}

