#ifndef OPENGLWINDOW_H
#define OPENGLWINDOW_H

#include "stdafx.h"
#include "Application.h"
#include "Timer.h"
#include "Input.h"

class OpenGLWindow
{
public:
	OpenGLWindow();
	~OpenGLWindow();

	void Init( int* _argc, char* _argv[ ] );

	void RenderCB( );
	void TimerCB( int _value );
	void IdleCB();

	static void RenderWrapper();
	static void TimerWrapper( int _value );
	static void IdleWrapper();

private:

	void InitGlutWindow( int* _argc, char* _argv[ ] );
	void InitGlew();

private:

	Application m_application;
	Timer m_timer;
	Input m_input;

	glm::ivec2 m_winSize;
	glm::ivec2 m_winPos;
	char* m_winName;

	glm::vec4 m_backgroundColor;

	unsigned int m_maxFPS;

};

static OpenGLWindow* g_instance;

#endif