#include "Object.h"

Object::Object( )
{
	m_position	= glm::vec3( 0.0f, 0.0f, 0.0f );
	m_direction = glm::vec3( 0.0f, 0.0f, 1.0f );
}

Object::~Object( )
{
}

void Object::Init( )
{

}

void Object::Update( float _dt )
{

}

void Object::Render( )
{

}