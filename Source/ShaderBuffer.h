#pragma once
#include "stdafx.h"

template<typename Element>
class ShaderBuffer
{
public:
	ShaderBuffer() {};
	~ShaderBuffer() {};

	virtual void Init( unsigned int _capacity )
	{
		m_capacity = _capacity;
		m_handle = 0;
		glDeleteBuffers( 1, &m_handle );
		glGenBuffers( 1, &m_handle );
		glBindBuffer( m_bufferType, m_handle ); /* Bind the buffer to be able to change it.*/
		glBufferData( m_bufferType, _capacity * sizeof( Element ), NULL, GL_DYNAMIC_DRAW ); /* Set the specifics for the SSBO.*/
		glBindBuffer( m_bufferType, 0 ); /* Deactivate the buffer not to change it by accident.*/
	};

	virtual void Bind( GLuint _program, std::string _nameInShader, GLuint _indexInShader ) = 0;

	void Update( Element* _data, unsigned int _noOfElements )
	{
		glBindBuffer( m_bufferType, m_handle );
		glBufferSubData( m_bufferType, 0, _noOfElements * sizeof( Element ), _data );
	}

protected:
	int						m_bufferType;
	GLuint					m_handle;
	unsigned int			m_capacity;
};

