#ifndef ObjectManager_h
#define ObjectManager_h

#include "stdafx.h"
#include "Object.h"
#include "RayHelper.h"
#include "LightHelper.h"
#include "ControlledUnit.h"
#include "ShaderStorageBuffer.h"
#include "ObjModel.h"

class ObjectManager
{
public:
	ObjectManager( );
	~ObjectManager( );

	void Init( GLuint _program );
	void Update( float _dt );
	void Render();

	int GetTextureId( std::string _textureName );

	void SetKeysPressedArrayPointer( const bool* const _keysPressed ) { m_controlledUnit.SetKeysPressedArrayPointer( _keysPressed ); };
	void SetMouseButtonsPressedArrayPointer( const bool* const _buttonsPressed ) { m_controlledUnit.SetMouseButtonsPressedArrayPointer( _buttonsPressed ); };
	void SetMousePosPointer( const glm::ivec2* const _mousePos ) { m_controlledUnit.SetMousePosPointer( _mousePos ); };

	ControlledUnit* GetControlledUnit() { return &m_controlledUnit; };
	std::map<std::string, unsigned int> GetTextures() { return m_textures; };
	
private:

	ShaderStorageBuffer<Triangle>	m_triangleBuffer;
	ShaderStorageBuffer<Sphere>		m_sphereBuffer;
	ShaderStorageBuffer<PointLight>	m_pointLightBuffer;

	std::map<std::string, unsigned int> m_textures;
	//std::vector<Object*> m_objects;

	ControlledUnit m_controlledUnit;

	ObjModel m_obj;
};

#endif