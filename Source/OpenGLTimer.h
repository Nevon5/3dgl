#pragma once

#include "stdafx.h"

class OpenGLTimer
{
public:
	OpenGLTimer();
	~OpenGLTimer();

	void Init();
	void Start();
	void Stop();

	GLuint64 GetTimeTaken( ) const;

private:
	
	GLuint m_startQuery, m_endQuery;
};