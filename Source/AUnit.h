#ifndef AUnit_h
#define AUnit_h

#include "stdafx.h"
#include "IUnitObserver.h"
#include <vector>

/* Not actually abstract, but not supposed to be instantiated.*/
class AUnit
{
public:
	void AttachUO( IUnitObserver* _observer );
	void DetachUO( IUnitObserver* _observer );
	void NotifyUO( glm::vec3 _pos, glm::vec3 _dir );

private:
	std::vector<IUnitObserver*> m_UoList;

};

#endif