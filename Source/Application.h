#ifndef APPLICATION_H
#define APPLICATION_H

#include "stdafx.h"
#include "Camera.h"
#include "ObjectManager.h"
#include "ShaderStorageBuffer.h"
#include "UniformBuffer.h"
#include "OpenGLTimer.h"

class Application
{
public:
	Application();
	~Application();

	void Init(glm::ivec2 _winSize);
	void LoadTextures();
	void Update(float _dt);
	void Render();

	Camera* GetCamera() { return &m_camera; };
	void SetKeysPressedArrayPointer(const bool* const _keysPressed) { m_camera.SetKeysPressedArrayPointer(_keysPressed); m_objectManager.SetKeysPressedArrayPointer(_keysPressed); };
	void SetMouseButtonsPressedArrayPointer(const bool* const _buttonsPressed) { m_objectManager.SetMouseButtonsPressedArrayPointer(_buttonsPressed); };
	void SetMousePosPointer(const glm::ivec2* const _mousePos) { m_objectManager.SetMousePosPointer(_mousePos); };

private:

	void Raytrace();
	void RenderComputeTexture();

	void CompileShaders();
	void AddShader(GLuint _shaderProgram, const char* _shaderPath, GLenum _shaderType, const char** _includePaths = 0, int _nrOfIncludes = 0);
	std::string LoadFileToString(const char* _shaderPath, int* _noOfRows);
	//void LoadFileToString( const char* _shaderPath, std::string* _code );
	void CreateProgram(GLuint* _program);
	void CheckProgram(GLuint _program);
	void GenerateTexture(glm::ivec2 _size);
	void CreateFullscreenQuad();


private:

	GLuint m_renderVBO;
	GLuint m_renderProgram;
	//GLuint m_computeProgram;
	GLuint m_primaryRaysProgram;
	GLuint m_intersectionProgram;
	GLuint m_colorProgram;

	glm::uvec2 m_workGroups;

	/* Handle to the texture being rendered in the compute shader.*/
	GLuint m_computeTexture;

	Camera m_camera;
	OpenGLTimer m_timer;
	ObjectManager m_objectManager;

	ShaderStorageBuffer<Ray>			m_rayBuffer;
	ShaderStorageBuffer<Intersection>	m_intersectBuffer;
	UniformBuffer<PerFrame>				m_bufferPerFrame;

	PerFrame	m_perFrame;

	int	m_noOfPasses;
};

#endif