#include "AKeyboard.h"

void AKeyboard::AttachKO( IKeyboardObserver* _observer )
{
	m_KoList.push_back( _observer );
}

void AKeyboard::DetachKO( IKeyboardObserver* _observer )
{
	if ( m_KoList.size( ) < 1 )
	{
		return;
	}

	std::vector<IKeyboardObserver*>::iterator i = m_KoList.begin( );

	for ( ; i != m_KoList.end( ); i++ )
	{
		if ( *i == _observer )
		{
			m_KoList.erase( i );
			return;
		}
	}
}

void AKeyboard::NotifyKO( unsigned char _key, int _x, int _y, KeyNotification _type )
{
	if ( m_KoList.size( ) < 1 )
	{
		return;
	}

	for ( size_t i = 0; i < m_KoList.size( ); ++i )
	{
		// If the observer exist, notify that a key has been pressed/released.
		if ( m_KoList[ i ] != 0 )
		{
			switch ( _type )
			{
			case AKeyboard::KeyPressed:
				m_KoList[ i ]->KeyPressed( _key, _x, _y );
				break;

			case AKeyboard::KeyReleased:
				m_KoList[ i ]->KeyReleased( _key, _x, _y );
				break;
			}
		}
	}
}