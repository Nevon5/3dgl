#ifndef AKEYBOARD_H
#define AKEYBOARD_H

#include "IKeyboardObserver.h"
#include <vector>

/* Not actually abstract, but not supposed to be instantiated.*/
class AKeyboard
{
public:
	enum KeyNotification
	{
		KeyPressed,
		KeyReleased
	};

public:
	void AttachKO( IKeyboardObserver* _observer );
	void DetachKO( IKeyboardObserver* _observer );
	void NotifyKO( unsigned char _key, int _x, int _y, KeyNotification _type );

private:
	std::vector<IKeyboardObserver*> m_KoList;

};

#endif