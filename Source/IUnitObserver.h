#ifndef IUnitObserver_h
#define IUnitObserver_h

class IUnitObserver
{
public:
	virtual void UnitMoved( glm::vec3 _pos, glm::vec3 _dir ) = 0;
};

#endif