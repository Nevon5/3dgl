#ifndef IMOUSEOBSERVER_H
#define IMOUSEOBSERVER_H

class IMouseObserver
{
public:
	virtual void MouseButton( int _button, int _state, int _x, int _y ) = 0;
	virtual void MouseMove( int _x, int _y ) = 0;
	virtual void MouseMovePassive( int _x, int _y ) = 0;
};

#endif