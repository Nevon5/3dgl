#include "ObjModel.h"
#include <sstream>

ObjModel::ObjModel(){}

ObjModel::~ObjModel(){}

std::string ObjModel::Load(char* _filename, unsigned int _startingMatId)
{
	m_startingMatId = _startingMatId;

	/* Create an input file stream to read the file.*/
	std::ifstream f;
	std::string str;
	std::string texture;

	f.open(_filename);

	if (!f)
	{
		printf("Can't find OBJ-file %s\n", _filename);
	}

	/* Parse the file until the end of the file has been reached.*/
	while(!f.eof())
	{
		f >> str;

			 if (str == "#" || str == "s")	ParseComment(f);
		else if (str == "v")				ParsePosition(f);
		else if (str == "vt")				ParseTexCoord(f);
		else if (str == "vn")				ParseNormal(f);
		else if (str == "g")				ParseGroup(f);
		else if (str == "f")				ParseFace(f);
		else if (str == "usemtl")			ParseMaterial(f);

		else if (str == "mtllib")
		{
			std::string dir = _filename;
			dir = dir.substr(0, dir.find_last_of("/") + 1);
			texture = ParseMaterialFile(f, dir);
		}

		str = "";
	}
	return texture;
}

void ObjModel::ParsePosition(std::ifstream& f)
{
	glm::vec3 pos;

	f >> pos.x;
	f >> pos.y;
	f >> pos.z;

	m_positions.push_back(pos);
}

void ObjModel::ParseTexCoord(std::ifstream& f)
{
	glm::vec2 tex;

	f >> tex.x;
	f >> tex.y;

	m_texCoords.push_back(tex);
}

void ObjModel::ParseNormal(std::ifstream& f)
{
	glm::vec3 norm;

	f >> norm.x;
	f >> norm.y;
	f >> norm.z;

	m_normals.push_back(norm);
}

void ObjModel::ParseFace(std::ifstream& f)
{
	std::vector<glm::vec4> indices;
	/* First value is position, second texcoord and third is normal.*/
	glm::vec4 index;
	char dump;
	std::string str;
	std::stringstream ss;
	
	/* Get next line in stream.*/
	std::getline(f, str);
	ss << str;

	index.w = m_currentMatId;
	/* Get indices until the end of the line.*/
	while ( ss >> index.x )
	{
		ss >> dump;
		ss >> index.y;
		ss >> dump;
		ss >> index.z;
		indices.push_back(index);
	}

	/* The line contains a single triangle.*/
	if ( indices.size() == 3 )
	{
		for (size_t i = 0; i < 3; i++)
		{
			m_indices.push_back(indices[i]);
		}
	}
	/* The line contains a quad (two triangles).*/
	else if ( indices.size() == 4 )
	{
		///* The first three indices of the quad is used to create a triangle.*/
		//for (size_t i = 0; i < 3; i++)
		//{
		//	m_indices.push_back(indices[i]);
		//}
		///* The last three indices of the quad is used to create a triangle.*/
		//for (size_t i = 1; i < 4; i++)
		//{
		//	m_indices.push_back(indices[i]);
		//}
		m_indices.push_back( indices[ 0 ] );	
		m_indices.push_back( indices[ 1 ] );		
		m_indices.push_back( indices[ 2 ] );		
		
		m_indices.push_back( indices[ 0 ] );		
		m_indices.push_back( indices[ 3 ] );		
		m_indices.push_back( indices[ 2 ] );

		//m_indices.push_back( indices[ 0 ] );	m_indices.push_back( indices[ 1 ] );	m_indices.push_back( indices[ 3 ] );		m_indices.push_back( indices[ 1 ] );		m_indices.push_back( indices[ 2 ] );		m_indices.push_back( indices[ 3 ] );
	}
	else
	{
		assert("ObjModel - Face setup not recognized.");
	}
}

void ObjModel::ParseMaterial(std::ifstream& f)
{
	std::string str;
	f >> str;
	m_currentMatId = m_materialIds[str] + m_startingMatId;
	//m_currentMat = Material(current.Ambient, current.Diffuse, current.Specular);
}

void ObjModel::ParseGroup(std::ifstream& f)
{

}

void ObjModel::ParseComment(std::ifstream& f)
{
	//std::string str;
	//std::getline(f, str);
}

std::string ObjModel::ParseMaterialFile(std::ifstream& f, std::string fileDir)
{
	ObjModel::ObjMaterial data;
	std::string materialName;
	std::string str;
	std::ifstream fmtl;

	f >> str;
	fmtl.open(fileDir + str);

	if (!fmtl)
	{
		printf("Can't find mtl-file %s%s", fileDir.c_str(), str.c_str());
	}

	bool firstMat = true;

	/* Parse each line until the end of the file has been reached.*/
	while (!fmtl.eof())
	{
		fmtl >> str;

		if (str == "newmtl")
		{
			if (!firstMat)
			{
				//m_materials.insert(std::make_pair(materialName, data));
				m_materialIds.insert(std::make_pair(materialName, m_materials.size()));
				m_materials.push_back(Material(data.Ambient, data.Diffuse, data.Specular));
			}

			fmtl >> materialName;
			firstMat = false;
		}
		else if (str == "illum")
		{
			fmtl >> data.IlluminationModel;
		}
		else if (str == "Kd")
		{
			fmtl >> data.Diffuse.x;
			fmtl >> data.Diffuse.y;
			fmtl >> data.Diffuse.z;
		}
		else if (str == "Ka")
		{
			fmtl >> data.Ambient.x;
			fmtl >> data.Ambient.y;
			fmtl >> data.Ambient.z;
		}
		else if (str == "Ks")
		{
			fmtl >> data.Specular.x;
			fmtl >> data.Specular.y;
			fmtl >> data.Specular.z;
		}
		else if (str == "Tf")
		{
			fmtl >> data.TransmissionFilter.x;
			fmtl >> data.TransmissionFilter.y;
			fmtl >> data.TransmissionFilter.z;
		}
		else if (str == "map_Kd")
		{
			fmtl >> str;
			data.TextureMap = fileDir + str;
		}
		else if (str == "Ni")
		{
			fmtl >> data.OpticalDensity;
		}
		
		str = "";
	}

	//m_materials.insert(std::make_pair(materialName, data));
	m_materialIds.insert(std::make_pair(materialName, m_materials.size()));
	m_materials.push_back(Material(data.Ambient, data.Diffuse, data.Specular));

	return data.TextureMap;
}

void ObjModel::SaveToMesh(float p_SizeRatio, int p_textureId )
{
	m_textureId = p_textureId;
	Triangle triangle;

	for (size_t index = 0; index < m_indices.size(); index += 3)
	{
		triangle.Vertices[0] = Vertex(GetPosition(index) * p_SizeRatio, GetMaterialId(index), GetNormal(index), m_textureId, GetTexCoord(index) );
		triangle.Vertices[1] = Vertex(GetPosition(index + 1) * p_SizeRatio, GetMaterialId(index), GetNormal(index + 1), m_textureId, GetTexCoord(index + 1));
		triangle.Vertices[2] = Vertex(GetPosition(index + 2) * p_SizeRatio, GetMaterialId(index), GetNormal(index + 2), m_textureId, GetTexCoord(index + 2));
		m_triangles.push_back(triangle);
	}
}

//void ObjModel::SaveToMesh(float p_SizeRatio)
//{
//	bool add;
//	/* previouslySkipped keeps track of how many Vertices that have been 
//	skipped in previous iterations. */
//	unsigned int previouslySkipped = 0; 
//
//	/* Loop through the indices face-wise, i.e. every sixth index.*/
//	for (unsigned int i = 0; i < m_indices.size(); i += 6)
//	{
//		unsigned int index = i;
//		unsigned int skipped = 0;
//
//		for (unsigned int j = i; j < i + 6; j++)
//		{
//			unsigned int k;
//			add = true;
//			for (k = i; k < j; k++)
//			{
//				index = CompareIndices(k, j);
//
//				if (index == k) /* This Vertex is a double, do not add it.*/
//				{
//					add = false;
//					break;
//				}
//			}
//
//			/* If this is a new Vertex, push it to the vertices vector
//			and push its index to the indices vector, which will always be the
//			size of the vertices vector as it is pushed to the end of the 
//			vector. */
//			if (add)
//			{
//				m_Mesh.Indices.push_back(m_Mesh.Vertices.size());
//				m_Mesh.Vertices.push_back(Vertex(
//					glm::vec3(	GetPosition(index).x * p_SizeRatio,
//								GetPosition(index).y * p_SizeRatio,
//								GetPosition(index).z * p_SizeRatio),
//					GetNormal(index), 
//					GetTexCoord(index)));
//			}
//			/* Else, push the index of the Vertex which this is a copy of and
//			leave the vertices vector as it is. */
//			else
//			{
//				m_Mesh.Indices.push_back(index - previouslySkipped);
//				skipped++;
//			}
//		}
//		previouslySkipped += skipped;
//	}
//}

/* Used when saving the mesh, skipping doubles of Vertices.
Compare the positions of two saved positions from the obj. If they are 
the same, return the first parameter, else return the second. This decides 
which position to use, if the second is a double, it is a copy and it is 
thrown away. */
//int ObjModel::CompareIndices(int i, int j)
//{
//	if (GetPosition(i).x != GetPosition(j).x ||
//		GetPosition(i).y != GetPosition(j).y ||
//		GetPosition(i).z != GetPosition(j).z)
//		return j;
//	else
//		return i;
//}

//ObjModel::MeshData ObjModel::GetMesh()
//{
//	return m_Mesh;
//}

glm::vec3 & ObjModel::GetPosition(unsigned int _index)
{
	return m_positions[(unsigned int)m_indices[_index].x - 1];
}

glm::vec2 & ObjModel::GetTexCoord(unsigned int _index)
{
	return m_texCoords[(unsigned int)m_indices[_index].y - 1];
}

glm::vec3 & ObjModel::GetNormal(unsigned int _index)
{
	//return glm::vec3( 0.0f, 0.0f, 1.0f );
	return m_normals[(unsigned int)m_indices[_index].z - 1];
}

int ObjModel::GetMaterialId(unsigned int _index)
{
	return (int)m_indices[_index].w;
}