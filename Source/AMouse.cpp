#include "AMouse.h"

void AMouse::AttachMO( IMouseObserver* _observer )
{
	m_MoList.push_back( _observer );
}

void AMouse::DetachMO( IMouseObserver* _observer )
{
	if ( m_MoList.size( ) < 1 )
	{
		return;
	}

	std::vector<IMouseObserver*>::iterator i = m_MoList.begin( );

	for ( ; i != m_MoList.end( ); i++ )
	{
		if ( *i == _observer )
		{
			m_MoList.erase( i );
			return;
		}
	}
}

void AMouse::NotifyMO( int _button, int _state, int _x, int _y, MouseNotification _type )
{
	if ( m_MoList.size( ) < 1 )
	{
		return;
	}

	for ( size_t i = 0; i < m_MoList.size( ); ++i )
	{
		// If the observer exist, notify that the mouse has been used.
		if ( m_MoList[ i ] != 0 )
		{
			switch ( _type )
			{
			case AMouse::MouseMove:
				m_MoList[ i ]->MouseMove( _x, _y );
				break;

			case AMouse::MouseMovePassive:
				m_MoList[ i ]->MouseMovePassive( _x, _y );
				break;

			case AMouse::MouseButton:
				m_MoList[ i ]->MouseButton( _button, _state, _x, _y );
				break;
			}
		}
	}
}