#pragma once
#include "stdafx.h"
#include "ShaderBuffer.h"

template<typename Element>
class UniformBuffer : public ShaderBuffer<Element>
{
public:
	UniformBuffer() {};
	~UniformBuffer() {};

	void Init( unsigned int _capacity )
	{
		m_bufferType = GL_UNIFORM_BUFFER;
		ShaderBuffer::Init( _capacity );
	};

	void Bind( GLuint _program, std::string _nameInShader, GLuint _indexInShader )
	{
		GLuint bindPoint = glGetUniformBlockIndex( _program, _nameInShader.c_str() );
		glUniformBlockBinding( _program, bindPoint, _indexInShader ); /* Connect the uniform block to the UBO. (Not needed if it is hard coded in the shader.)*/
		glBindBufferBase( GL_UNIFORM_BUFFER, _indexInShader, m_handle ); /* Binding the UBO to a particular binding point.*/
	};
};

