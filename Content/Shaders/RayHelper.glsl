#version 430

#define EPSILON 0.0001f

struct Ray
{
	vec3 Origin;		
	bool NoBounce;
	vec3 Direction;		int pad1;
};

struct RayCollision
{
	float Distance;
	float U;
	float V;
};

struct Intersection
{
	vec3	Normal;		int pad1;
	vec3	Position;
	int		PassNo;
	vec4	Color;
	vec4	AmbientDiffuse;
	vec4	Specular;
};

struct Vertex
{
	vec3	Position;
	int		MaterialId;
	vec3	Normal;	
	int		TextureId;
	vec2	TexCoord;		vec2 pad2;
	//vec4 Color;
};

struct Triangle
{
	Vertex Vertices[3];
};

struct Sphere
{
	vec3	Center;
	float	Radius;
	vec4	Color;
};

struct PerFrame
{
	vec3 CamPos;		float pad1;
	vec3 CamLook;		float pad2;
	vec3 CamUp;			float pad3;
	vec3 CamRight;		float pad4;
};

struct PointLight;

uniform ivec2 g_winSize;
uniform vec2 g_fovFactor;
uniform float g_farPlane;
uniform int g_passNo;

layout (rgba32f) uniform image2D g_destTex;

layout (binding = 2) uniform sampler2DArray g_textures;

layout (std430, binding = 0) buffer TriangleBuffer
{
	Triangle g_triangles[];
};

layout (std430, binding = 1) buffer SphereBuffer
{
	Sphere g_spheres[];
};

layout (std430, binding = 2) buffer RayBuffer
{
	Ray g_rays[];
};

layout (std430, binding = 3) buffer IntersectionBuffer
{
	Intersection g_intersects[];
};

layout (std430, binding = 4) buffer PointLightBuffer
{
	PointLight g_pointLights[];
};

layout (std140) uniform BufferPerFrame
{
	vec3 g_camPos;		float pad1;
	vec3 g_camLook;		float pad2;
	vec3 g_camUp;		float pad3;
	vec3 g_camRight;	float pad4;
};

RayCollision RayVsTriangle(Ray _ray, Triangle _triangle)
{
	RayCollision collision;
	vec3 edge1 = _triangle.Vertices[2].Position.xyz - _triangle.Vertices[0].Position.xyz;
	vec3 edge2 = _triangle.Vertices[1].Position.xyz - _triangle.Vertices[0].Position.xyz;
	vec3 q = cross( _ray.Direction, edge2 );
	float a = dot( edge1, q );

	/* If a is zero (or close to zero), the ray is parallel to the triangles "plane".*/
	if ( a > -0.00001f && a < 0.00001f )
	{
		collision.Distance = -1.0f;
		return collision;
	}

	float f = 1.0f / a;
	vec3 toTriangle = _ray.Origin - _triangle.Vertices[0].Position.xyz;
	float u = f * dot( toTriangle, q );

	/* The triangle is behind the ray.*/
	if ( u < 0.0f )
	{
		collision.Distance = -1.0f;
		return collision;
	}

	vec3 r = cross( toTriangle, edge1 );
	float v = f * dot( _ray.Direction, r );

	/* The ray is outside the triangles boundaries.*/
	if ( v < 0.0f || u + v > 1.0f )
	{
		collision.Distance = -1.0f;
		return collision;
	}

	collision.Distance = f * dot( edge2, r );
	collision.U = u;
	collision.V = v;

	return collision;
}

bool RayVsSphere( Ray _ray, Sphere _sphere, float _distToLight )
{
	vec3 toSphereCenter			= _sphere.Center - _ray.Origin;
	float distSquared			= pow( length( toSphereCenter ), 2.0f );
	float radiusSquared			= pow( _sphere.Radius, 2.0f );
	bool rayOriginInsideSphere	= distSquared < radiusSquared;
	float projected				= dot( toSphereCenter, _ray.Direction );

	if ( projected >= 0.0f && !rayOriginInsideSphere )
	{
		float distFromCenterSquared = distSquared - pow( projected, 2.0f );

		if ( distFromCenterSquared <= radiusSquared )
		{
			float intersectionDistance = sqrt( radiusSquared - distFromCenterSquared );

			if ( projected - abs(intersectionDistance) < _distToLight )
			{
				return true;
			}
		}
	}
	
	return false;
}

float RayVsSphere( Ray _ray, Sphere _sphere )
{
	vec3 toSphereCenter			= _sphere.Center - _ray.Origin;
	float distSquared			= pow( length( toSphereCenter ), 2.0f );
	float radiusSquared			= pow( _sphere.Radius, 2.0f );
	bool rayOriginInsideSphere	= distSquared < radiusSquared;
	float projected				= dot( toSphereCenter, _ray.Direction );

	if ( projected >= 0.0f && !rayOriginInsideSphere )
	{
		float distFromCenterSquared = distSquared - pow( projected, 2.0f );

		if ( distFromCenterSquared <= radiusSquared )
		{
			float intersectionDistance = sqrt( radiusSquared - distFromCenterSquared );

			if ( projected - intersectionDistance < projected + intersectionDistance )
			{
				return projected - intersectionDistance;
			}
			else
			{
				return projected + intersectionDistance;
			}
		}
	}
	
	return -1.0f;
}

bool RayVsAABB( Ray _ray, vec3 _bounds[2] )
{
	vec3 invdir = 1.0f / _ray.Direction.xyz;

	float t1 = ( _bounds[0].x - _ray.Origin.x ) * invdir.x;
	float t2 = ( _bounds[1].x - _ray.Origin.x ) * invdir.x;
	float t3 = ( _bounds[0].y - _ray.Origin.y ) * invdir.y;
	float t4 = ( _bounds[1].y - _ray.Origin.y ) * invdir.y;
	float t5 = ( _bounds[0].z - _ray.Origin.z ) * invdir.z;
	float t6 = ( _bounds[1].z - _ray.Origin.z ) * invdir.z;

	float tmin = max( max( min( t1, t2 ), min( t3, t4 ) ), min( t5, t6 ) );
	float tmax = min( min( max( t1, t2 ), max( t3, t4 ) ), max( t5, t6 ) );

	if ( tmax < 0 || tmin > tmax )
		return false;

	return true;
}

//bool RayVsTriangle(Ray _ray, Triangle _triangle, float _distToLight)
//{
//	vec3 edge1 = _triangle.Vertices[1].Position.xyz - _triangle.Vertices[0].Position.xyz;
//	vec3 edge2 = _triangle.Vertices[2].Position.xyz - _triangle.Vertices[0].Position.xyz;
//	vec3 q = cross( _ray.Direction, edge2 ); /* Normal between ray and second edge.*/
//	float a = dot( edge1, q ); /* "Angle" between first edge and the normal between ray and second edge */

//	/* If a is zero (or close to zero), the ray is parallel to the triangles "plane".*/
//	if ( -0.00001f < a && a < 0.00001f )
//	{
//		return false;
//	}

//	float f = 1.0f / a;
//	vec3 toTriangle = _ray.Origin - _triangle.Vertices[0].Position.xyz;
//	float u = f * dot( toTriangle, q );

//	/* The triangle is behind the ray.*/
//	if ( u < 0.0f )
//	{
//		return false;
//	}

//	vec3 r = cross( toTriangle, edge1 );
//	float v = f * dot( _ray.Direction, r );

//	/* The ray is outside the triangles boundaries.*/
//	if ( v < 0.0f || 1.0f < u + v )
//	{
//		return false;
//	}

//	float dist = f * dot( edge2, r );

//	/* The triangle is further away than the light.*/
//	if ( _distToLight < dist )
//	{
//		return false;
//	}

//	return true;
//}