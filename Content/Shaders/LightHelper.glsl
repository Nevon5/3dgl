struct Material
{
	vec4 Ambient;
	vec4 Diffuse;
	vec4 Specular;	/* The w-component defines the Specular power, which is how large the specular cone will be, small for a polished surface and large for a rough surface.*/
	//vec4 Reflect;
};

struct DirectionalLight
{
	vec4	Ambient;
	vec4	Diffuse;
	vec4	Specular;
	vec3	Direction;	float pad;
};

struct PointLight
{ 
	vec4	Ambient;
	vec4	Diffuse;
	vec4	Specular;
	vec3	Position;
	float	Range;
	vec3	Att;		float pad;
};

struct SpotLight
{
	vec4	Ambient;
	vec4	Diffuse;
	vec4	Specular;
	vec3	Position;
	float	Range;
	vec3	Direction;
	float	Spot;
	vec3	Att;		float pad;
};

//---------------------------------------------------------------------------------------
// Computes the ambient, diffuse, and specularular terms in the lighting equation
// from a directional light.  We need to output the terms separately because
// later we will modify the individual terms.
//---------------------------------------------------------------------------------------
void ComputeDirectionalLight(Material _mat, DirectionalLight _light, 
                             vec3 _normal, vec3 _toCam,
					         out vec4 ambient,
						     out vec4 diffuse,
						     out vec4 specular)
{
	// Initialize outputs.
	ambient = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	specular    = vec4(0.0f, 0.0f, 0.0f, 0.0f);

	// The light vector aims opposite the direction the light rays travel.
	vec3 lightVec = -_light.Direction;

	// Add ambient term.
	ambient = _mat.Ambient * _light.Ambient;	

	// Add diffuse and specularular term, provided the surface is in the line of site of the light.
	
	float diffuseFactor = dot(lightVec, _normal);

	if( diffuseFactor > 0.0f )
	{
		vec3 v         = reflect(-lightVec, _normal);
		float specularFactor = pow(max(dot(v, _toCam), 0.0f), _mat.Specular.w);
					
		diffuse = diffuseFactor * _mat.Diffuse * _light.Diffuse;
		specular    = specularFactor * _mat.Specular * _light.Specular;
	}
}

//---------------------------------------------------------------------------------------
// Computes the ambient, diffuse, and specularular terms in the lighting equation
// from a point light.  We need to output the terms separately because
// later we will modify the individual terms.
//---------------------------------------------------------------------------------------
void ComputePointLight(Material _mat, PointLight _light, vec3 pos, vec3 _normal, vec3 _toCam,
				   out vec4 _ambient, out vec4 _diffuse, out vec4 _specular)
{
	// Initialize outputs.
	_ambient	= vec4(0.0f, 0.0f, 0.0f, 0.0f);
	_diffuse	= vec4(0.0f, 0.0f, 0.0f, 0.0f);
	_specular	= vec4(0.0f, 0.0f, 0.0f, 0.0f);

	// The vector from the surface to the light.
	vec3 lightVec = _light.Position - pos;
		
	// The distance from surface to light.
	float d = length(lightVec);
	
	// Range test.
	if( d > _light.Range )
		return;
		
	// Normalize the light vector.
	lightVec /= d; 
	
	// Ambient term.
	_ambient = _mat.Ambient * _light.Ambient;	

	// Add diffuse and specular term, provided the surface is in the line of sight of the light.

	float diffuseFactor = dot(lightVec, _normal);

	if( diffuseFactor > 0.0f )
	{
		vec3 reflection			= reflect(-lightVec, _normal);
		float specularFactor	= pow(max(dot(reflection, _toCam), 0.0f), _mat.Specular.w);
					
		_diffuse	= diffuseFactor * _mat.Diffuse * _light.Diffuse;
		_specular   = specularFactor * _mat.Specular * _light.Specular;
	}

	// Attenuate
	float att = 1.0f / dot(_light.Att, vec3(1.0f, d, d*d));

	_diffuse	*= att;
	_specular   *= att;
}

//---------------------------------------------------------------------------------------
// Computes the ambient, diffuse, and specularular terms in the lighting equation
// from a spotlight.  We need to output the terms separately because
// later we will modify the individual terms.
//---------------------------------------------------------------------------------------
void ComputeSpotLight(Material _mat, SpotLight _light, vec3 pos, vec3 _normal, vec3 _toCam,
				  out vec4 ambient, out vec4 diffuse, out vec4 specular)
{
	// Initialize outputs.
	ambient = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = vec4(0.0f, 0.0f, 0.0f, 0.0f);
	specular    = vec4(0.0f, 0.0f, 0.0f, 0.0f);

	// The vector from the surface to the light.
	vec3 lightVec = _light.Position - pos;
		
	// The distance from surface to light.
	float d = length(lightVec);
	
	// Range test.
	if( d > _light.Range )
		return;
		
	// Normalize the light vector.
	lightVec /= d; 
	
	// Ambient term.
	ambient = _mat.Ambient * _light.Ambient;	

	// Add diffuse and specular term, provided the surface is in the line of site of the light.

	float diffuseFactor = dot(lightVec, _normal);

	if( diffuseFactor > 0.0f )
	{
		vec3 v         = reflect(-lightVec, _normal);
		float specularFactor = pow(max(dot(v, _toCam), 0.0f), _mat.Specular.w);
					
		diffuse = diffuseFactor * _mat.Diffuse * _light.Diffuse;
		specular    = specularFactor * _mat.Specular * _light.Specular;
	}
	
	// Scale by spotlight factor and attenuate.
	float spot = pow(max(dot(-lightVec, _light.Direction), 0.0f), _light.Spot);

	// Scale by spotlight factor and attenuate.
	float att = spot / dot(_light.Att, vec3(1.0f, d, d*d));

	ambient		*= spot;
	diffuse		*= att;
	specular	*= att;
}