/* Defines the work group size.*/
layout ( local_size_x = 8, local_size_y = 8 ) in;

const vec4 darkRed		= vec4(0.4, 0.0, 0.0, 0.0);
const vec4 darkGreen	= vec4(0.0, 0.4, 0.0, 0.0);
const vec4 darkBlue		= vec4(0.0, 0.0, 0.4, 0.0);

const vec4 gray			= vec4(0.4, 0.4, 0.4, 0.0);
const vec4 darkYellow	= vec4(0.4, 0.4, 0.0, 0.0);
const vec4 darkPurple	= vec4(0.4, 0.0, 0.4, 0.0);
const vec4 marineBlue	= vec4(0.0, 0.4, 0.4, 0.0);

const vec4 black		= vec4(0.0, 0.0, 0.0, 0.0);
const vec4 white		= vec4(1.0, 1.0, 1.0, 1.0);

uniform int g_noOfPasses;

void main() 
{
	ivec2 storePos = ivec2(gl_GlobalInvocationID.xy);
	int oneDimStorePos = storePos.x + storePos.y * g_winSize.x;
	Intersection intersect = g_intersects[oneDimStorePos];
	int passNo = g_passNo;

	/* The ray hit something.*/
	if ( !g_rays[oneDimStorePos].NoBounce )
	{	
		vec4 ambient = black;
		vec4 diffuse = black;
		vec4 specular = black;

		Material mat;
		mat.Ambient = white;
		mat.Diffuse = white;
		mat.Specular = white;
		mat.Specular.w = 16;
		//mat.Reflect = white;

		vec4 A, D, S;
		Ray ray;
		ray.Origin = intersect.Position + intersect.Normal * EPSILON;

		bool lit;
		int noOfPointLights = g_pointLights.length();
		int noOfTriangles = g_triangles.length();
		int noOfSpheres = g_spheres.length();

		/* Go through all point lights to see if any primitives are lit. */
		for ( int pl = 0; pl < noOfPointLights; pl++ )
		{
			PointLight pointLight = g_pointLights[pl];
			lit = true;
			ray.Direction = pointLight.Position - intersect.Position;
			float distToLight = length(ray.Direction);
			ray.Direction /= distToLight;
		
			int s = 0;
			for ( int tri = 0; tri < noOfTriangles; tri++ )
			{
				float dist = RayVsTriangle( ray, g_triangles[tri] ).Distance;

				if ( 0.0f < dist && dist < distToLight )
				{
					s = noOfSpheres; /* In shadow, no need to continue with neither triangles nor spheres.*/ 
					lit = false;
					break;
				}
			}
		
			for ( ; s < noOfSpheres; s++ )
			{
				if ( RayVsSphere( ray, g_spheres[s], distToLight ) )
				{
					lit = false;
					break;
				}
			}

			if ( lit )
			{
				ComputePointLight(mat, pointLight, intersect.Position, intersect.Normal, normalize(g_camPos - intersect.Position), A, D, S);
				ambient += A;
				diffuse += D;
				specular += S;
			}
		}
	
		/* Add lighting, this fades away with more bounces. */
		intersect.AmbientDiffuse += (ambient + diffuse) / passNo;
		intersect.Specular += specular / passNo;

		if ( passNo < g_noOfPasses )
		{
			/* Prepare rays for bounce.*/
			g_intersects[oneDimStorePos] = intersect;

			Ray bounce = g_rays[oneDimStorePos];
			bounce.Origin = ray.Origin;
			bounce.Direction = reflect(bounce.Direction, intersect.Normal);
			bounce.pad1 += 1;
			g_rays[oneDimStorePos] = bounce;

			return;
		}
	}
	else if ( passNo < g_noOfPasses )
	{
		return;
	}

	vec4 litColor = intersect.Color * intersect.AmbientDiffuse + intersect.Specular;
	litColor.w = 1.0;

	imageStore(g_destTex, storePos, litColor);

	Intersection intersectReset;
	g_intersects[oneDimStorePos] = intersectReset;
}