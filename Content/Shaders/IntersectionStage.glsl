/* Defines the work group size.*/
layout ( local_size_x = 8, local_size_y = 8 ) in;

const vec4 darkRed		= vec4(0.4, 0.0, 0.0, 0.0);
const vec4 darkGreen	= vec4(0.0, 0.4, 0.0, 0.0);
const vec4 darkBlue		= vec4(0.0, 0.0, 0.4, 0.0);

const vec4 gray			= vec4(0.4, 0.4, 0.4, 0.0);
const vec4 darkYellow	= vec4(0.4, 0.4, 0.0, 0.0);
const vec4 darkPurple	= vec4(0.4, 0.0, 0.4, 0.0);
const vec4 marineBlue	= vec4(0.0, 0.4, 0.4, 0.0);

const vec4 black		= vec4(0.0, 0.0, 0.0, 0.0);

void main() 
{
	ivec2 storePos = ivec2(gl_GlobalInvocationID.xy);
	int oneDimStorePos = storePos.x + storePos.y * g_winSize.x;

	Ray ray = g_rays[oneDimStorePos];
	
	/* For second iteration and onwards. The initial ray did not hit anything.*/
	if ( ray.NoBounce )
	{
		return;
	}

	RayCollision currentHit, nearestHit;
	nearestHit.Distance = g_farPlane;
	int noOfTriangles = g_triangles.length();
	int idOfNearest = -1;
	int nearestType = 0;
		
	for ( int i = 0; i < noOfTriangles; i++ )
	{
		currentHit = RayVsTriangle(ray, g_triangles[i]);

		if ( 0.0f < currentHit.Distance && currentHit.Distance < nearestHit.Distance )
		{
			nearestHit = currentHit;
			nearestType = 1;
			idOfNearest = i;
		}
	}

	int noOfSpheres = g_spheres.length();
	float current;

	for ( int i = 0; i < noOfSpheres; i++ )
	{
		current = RayVsSphere(ray, g_spheres[i]);

		if ( 0.0f < current && current < nearestHit.Distance )
		{
			nearestHit.Distance = current;
			nearestType = 2;
			idOfNearest = i;
		}
	}

	Intersection intersect;
	intersect = g_intersects[oneDimStorePos];

	/* The ray intersects with a triangle.*/
	if (nearestType == 1)
	{
		Triangle nearest = g_triangles[idOfNearest];

		mat2x2 m;
		m[0] = nearest.Vertices[2].TexCoord - nearest.Vertices[0].TexCoord;
		m[1] = nearest.Vertices[1].TexCoord - nearest.Vertices[0].TexCoord;

		vec2 vec = m * vec2(nearestHit.U, nearestHit.V) + nearest.Vertices[0].TexCoord;

		vec4 color = texture(g_textures, vec3(vec.x, vec.y, nearest.Vertices[0].TextureId));

		intersect.Color += color / g_passNo;

		intersect.Position = ray.Origin + ray.Direction * nearestHit.Distance;
		
		//intersect.Normal = nearest.Vertices[0].Normal;
		
		intersect.Normal = normalize(
		nearest.Vertices[1].Normal * vec.x +
		nearest.Vertices[2].Normal * (vec.x + 1 - vec.y) +
		nearest.Vertices[0].Normal * (1 - vec.x - vec.y));

		//intersect.Normal = normalize(mix(nearest.Vertices[0].Normal, nearest.Vertices[1].Normal, vec.x) + mix(nearest.Vertices[1].Normal, nearest.Vertices[2].Normal, vec.y))
	}
	/* The ray intersects with a sphere.*/
	else if (nearestType == 2)
	{ 
		Sphere nearest = g_spheres[idOfNearest];

		intersect.Position = ray.Origin + ray.Direction * nearestHit.Distance;
		intersect.Normal = normalize(intersect.Position - nearest.Center);
		intersect.Color += nearest.Color / g_passNo;
	}
	/* The ray did not hit anything.*/
	else
	{
		g_rays[oneDimStorePos].NoBounce = true;
	}

	g_intersects[oneDimStorePos] = intersect;
}