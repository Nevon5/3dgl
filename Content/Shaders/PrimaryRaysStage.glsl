/* Defines the work group size.*/
layout ( local_size_x = 8, local_size_y = 8 ) in;

const vec4 darkRed		= vec4(0.4, 0.0, 0.0, 0.0);
const vec4 darkGreen	= vec4(0.0, 0.4, 0.0, 0.0);
const vec4 darkBlue		= vec4(0.0, 0.0, 0.4, 0.0);

const vec4 gray			= vec4(0.4, 0.4, 0.4, 0.0);
const vec4 darkYellow	= vec4(0.4, 0.4, 0.0, 0.0);
const vec4 darkPurple	= vec4(0.4, 0.0, 0.4, 0.0);
const vec4 marineBlue	= vec4(0.0, 0.4, 0.4, 0.0);

const vec4 black		= vec4(0.0, 0.0, 0.0, 0.0);

void main() 
{
	ivec2 storePos = ivec2(gl_GlobalInvocationID.xy);

	/* Transform the thread IDs to the range of [-0.5, 0.5] (without factor and aspect). */
	float x = (float(storePos.x) / float(g_winSize.x) - 0.5) * g_fovFactor.x;
	float y = (float(storePos.y) / float(g_winSize.y) - 0.5) * g_fovFactor.y;

	/* Create a point which the thread will shoot its ray through.*/
	vec3 imagePoint = g_camPos + g_camLook + x * g_camRight + y * g_camUp;
	
	Ray ray;
	
	/* Shoot ray from the camera position through the projected point.*/
	ray.Origin = g_camPos;
	ray.Direction = normalize(imagePoint - g_camPos);

	g_rays[g_winSize.x * storePos.y + storePos.x] = ray;
	




	//imageStore(g_destTex, storePos, vec4(g_rays[g_winSize.x * storePos.y + storePos.x].Direction, 0.0));

	//imageStore(g_destTex, storePos, vec4(ray.Direction, 0.0));



	return;
}

// Use shared variables, for example the camera-variables?